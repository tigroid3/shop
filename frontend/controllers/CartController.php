<?php
namespace frontend\controllers;

use common\components\AjaxFilter;
use common\models\Item;
use common\models\ItemImage;
use frontend\components\Controller;
use Yii;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class CartController
 * @package frontend\controllers
 */
class CartController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => [
                    'buy',
                    'change-count',
                    'del-checked',
                    'update'
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws HttpException
     */
    public function actionIndex()
    {
        $positions = Yii::$app->cart->getPositions();
        $randomItems = Item::find()
            ->joinWith('defaultImage')
            ->where(['IS NOT', ItemImage::tableName() . '.id', null])
            ->orderBy(new Expression('RAND()'))
            ->limit(5)
            ->all();

        return $this->render('index', [
            'positions' => $positions,
            'randomItems' => $randomItems,
        ]);
    }

    /**
     * @return bool
     */
    public function actionBuy()
    {
        $post = Yii::$app->request->post();
        $model = $this->findItem($post['itemId']);
        $count = 1;

        if (!empty($post['count'])) {
            $count = $post['count'];
        }

        Yii::$app->cart->put($model, $count);

        return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionDelete($id)
    {
        $model = $this->findItem($id);
        Yii::$app->cart->remove($model);

        $this->redirect(Url::toRoute('cart/index'));
    }

    /**
     * @return array
     */
    public function actionChangeCount()
    {
        $post = Yii::$app->request->post();

        if (!empty($post['itemId']) && !empty($post['count'])) {
            $model = $this->findItem($post['itemId']);
            Yii::$app->cart->update($model, $post['count']);

            return [
                'cost' => Yii::$app->cart->getPositionById($model->id)->getCost(),
                'totalCost' => Yii::$app->cart->getCost()
            ];
        }
    }

    /**
     * @return array
     */
    public function actionDelChecked()
    {
        $post = Yii::$app->request->post();

        if (!empty($post['ids'])) {
            foreach ($post['ids'] as $id) {
                $model = $this->findItem($id);

                if ($model) {
                    Yii::$app->cart->removeById($model->id);
                }
            }

            return [
                'totalCost' => Yii::$app->cart->getCost(),
                'totalCount' => Yii::$app->cart->getCount(),
            ];
        }
    }

    /**
     * @return array
     */
    public function actionUpdate()
    {
        return [
            'totalCost' => Yii::$app->cart->getCost(),
            'totalCount' => Yii::$app->cart->getCount(),
        ];
    }

    /**
     * @param $id
     * @return Item|InvalidParamException
     */
    public function findItem($id)
    {
        $model = Item::findOne($id);

        if (!$model) {
            throw new InvalidParamException(Yii::t('common', 'Товар не найден'));
        }

        return $model;
    }

}
