<?php
namespace frontend\controllers;

use frontend\components\Controller;
use yii\web\HttpException;

/**
 * Class NewsController
 * @package frontend\controllers
 */
class NewsController extends Controller
{
    /**
     * @return string
     * @throws HttpException
     */
    public function actionIndex()
    {

        return $this->render('index', [

        ]);
    }
}
