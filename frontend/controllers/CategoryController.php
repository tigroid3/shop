<?php
namespace frontend\controllers;

use common\models\Category;
use frontend\components\Controller;
use frontend\models\search\CategorySearch;
use frontend\models\search\ItemSearch;
use Yii;
use yii\web\HttpException;

/**
 * Class CategoryController
 * @package frontend\controllers
 */
class CategoryController extends Controller
{
    /**
     * @param $id
     * @return string
     */
    public function actionIndex($id = Category::ROOT_CATEGORY)
    {
        $modelSearch = new ItemSearch();
        $category = $this->findModel($id);
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams, $id);

        $modelCategorySearch = new CategorySearch();
        $dataProviderCategory = $modelCategorySearch->search(Yii::$app->request->queryParams, $id);


        $costFrom = 0;
        $costTo = 10000;
        $paramQuery = [];

        if (isset(Yii::$app->request->queryParams['ItemSearch'])) {
            $paramQuery = Yii::$app->request->queryParams['ItemSearch'];

            if (isset($paramQuery['from']) && isset($paramQuery['to'])) {
                $costFrom = $paramQuery['from'];
                $costTo = $paramQuery['to'];
            }
        }

        return $this->render('index', [
            'category' => $category,
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'costFrom' => $costFrom,
            'costTo' => $costTo,
            'dataProviderCategory' => $dataProviderCategory,
            'paramQuery' => $paramQuery
        ]);
    }

    /**
     * @param $id
     * @return static
     * @throws HttpException
     */
    public function findModel($id)
    {
        $model = Category::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Категория не найдена.'));
        }

        return $model;
    }
}
