<?php
namespace frontend\controllers;

use common\models\Page;
use frontend\components\Controller;
use yii\web\HttpException;

/**
 * Class PageController
 * @package frontend\controllers
 */
class PageController extends Controller
{
    /**
     * @param string $alias
     * @return string
     * @throws HttpException
     */
    public function actionIndex($alias = 'index')
    {
        if ($alias == 'index') {
            $this->layout = 'index';
        }

        $page = Page::find()
            ->byAlias($alias)
            ->one();

        if (!$page) {
            throw new HttpException(404, 'Страница не найдена.');
        }

        return $this->render('index', [
            'page' => $page,
        ]);
    }

    /**
     * @return string
     */
    public function actionContacts()
    {
        return $this->render('contacts', [
            'page' => $this->findPage('contacts'),
        ]);
    }

    /**
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about', [
            'page' => $this->findPage('about'),
        ]);
    }

    /**
     * @param $alias
     * @return Page|string
     */
    protected function findPage($alias)
    {
        $model = Page::find()->byAlias($alias)->one();

        if ($model) {
            $content = $model;
        } else {
            $content = "";
        }

        return $content;
    }
}
