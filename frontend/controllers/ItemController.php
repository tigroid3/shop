<?php
namespace frontend\controllers;

use common\models\Item;
use frontend\components\Controller;
use Yii;
use yii\web\HttpException;

/**
 * Class ItemController
 * @package frontend\controllers
 */
class ItemController extends Controller
{
    /**
     * @return string
     * @throws HttpException
     */
    public function actionIndex($id)
    {
        $model = $this->getModel($id);

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * @param integer $id
     * @return Item
     * @throws HttpException
     */
    protected function getModel($id)
    {
        $model = Item::find()->joinWith(['images', 'category'])->byId($id)->one();

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Позиция не найдена.'));
        }

        return $model;
    }
}
