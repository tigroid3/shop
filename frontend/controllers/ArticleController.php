<?php
namespace frontend\controllers;

use frontend\components\Controller;
use yii\web\HttpException;

/**
 * Class ArticleController
 * @package frontend\controllers
 */
class ArticleController extends Controller
{
    /**
     * @return string
     * @throws HttpException
     */
    public function actionIndex()
    {

        return $this->render('index', [

        ]);
    }
}
