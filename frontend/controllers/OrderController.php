<?php
namespace frontend\controllers;

use common\models\Options;
use common\models\Order;
use common\models\OrderItem;
use frontend\components\Controller;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class OrderController
 * @package frontend\controllers
 */
class OrderController extends Controller
{
    /**
     * @return string
     * @throws HttpException
     */
    public function actionIndex()
    {
        $positions = Yii::$app->cart->getPositions();

        if (!$positions) {
            throw new HttpException(404, Yii::t('common', 'Невозможно выполнить действие'));
        }

        $order = new Order();
        $post = Yii::$app->request->post();

        if ($post) {
            $order->load($post);
            $order->hash_session = Yii::$app->cart->session->id;

            if ($order->save()) {
                foreach ($positions as $position) {
                    $itemOrder = new OrderItem();
                    $itemOrder->order_id = $order->id;
                    $itemOrder->item_id = $position->id;
                    $itemOrder->quantity = $position->quantity;
                    $itemOrder->cost = $position->price;
                    $itemOrder->save();
                }

                $this->redirect(Url::toRoute('/order/final'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($order);
            }
        };

        return $this->render('index', [
            'order' => $order
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionFinal()
    {
        $sessionId = Yii::$app->cart->session->id;

        $order = Order::find()
            ->joinWith('orderItems')
            ->where(['hash_session' => $sessionId])
            ->one();

        if ($order) {
            Yii::$app->cart->removeAll();

            $order->hash_session = null;
            $order->save();

            $this->sendMail($order);

            return $this->render('final', [
                'order' => $order
            ]);
        } else {
            return $this->redirect('/');
        }
    }

    /**
     * @param $order
     * @return bool
     */
    public function sendMail($order)
    {
        try {
            Yii::$app->mailer->compose('@common/mail/newOrder', [
                'order' => $order
            ])
                ->setFrom(Yii::$app->params['noReplyEmail'])
                ->setTo(Options::getEmails())
                ->setSubject('Новый заказ')
                ->send();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
