<?php
namespace frontend\controllers;

use common\models\Category;
use common\models\Item;
use common\models\Page;
use frontend\components\Controller;

/**
 * Class HomeController
 * @package frontend\controllers
 */
class HomeController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = Item::findOne(2);

        print_r($query->getFullUrl());
        die();
//        print_r('<br><br>');
//        print_r(MyHelper::buildCategoryTree($query));die();


        $page = Page::find()->byAlias('index')->one();

        return $this->render('index', [
            'page' => $page
        ]);
    }
}
