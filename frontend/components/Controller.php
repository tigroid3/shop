<?php
namespace frontend\components;

/**
 * Class Controller
 * @package frontend\components
 */
class Controller extends \yii\web\Controller
{
    public $layout = 'common';
}
