<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var string $id */
/** @var string $name */
/** @var string $url */
/** @var string $tagContent */
/** @var string $classItem */
/** @var string $activeClass */
?>

<?php $content = ($tagContent != '' ? Html::tag($tagContent, $name) : $name) ?>
<?php $icon = '<span class="badge"><i class="fa fa-plus"></i></span>' ?>

<?php if ($url) : ?>
    <?= Html::a($content, Url::to($url), ['id' => "category_item_$id", 'class' => "$activeClass $classItem"]) ?>
<?php else: ?>
    <?= Html::tag('span', $content, ['id' => "category_item_$id", 'class' => $classItem]) ?>
<?php endif; ?>

