<?php
use yii\helpers\Html;

/** @var string $id */
/** @var string $classTree */
/** @var string $tree */

?>

<?= Html::tag('ul', $tree, ['id' => $id, 'class' => $classTree]) ?>
