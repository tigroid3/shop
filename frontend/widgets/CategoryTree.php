<?php
namespace frontend\widgets;

use common\models\Category;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\base\Widget;

/**
 * Class CategoryTree
 * @package frontend\widgets
 */
class CategoryTree extends Widget
{
    public $classTree;

    public $item = 'id';

    public $parent = 'parent_id';

    public $name = 'name';

    public $classItem = 'class';

    public $tagContent = '';

    public $rootCategory = 1;

    public $sort = "sort";

    /** @var bool */
    public $aWithChild = false;

    /** @var Category[] */
    public $items;

    public function init()
    {
        parent::init();

        if ($this->sort != "") {
            usort($this->items, function ($a, $b) {
                return strnatcmp($b[$this->sort], $a[$this->sort]);
            });
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('category-tree', [
            'id' => $this->id,
            'classTree' => $this->classTree,
            'tree' => $this->buildTree($this->items)
        ]);
    }

    /**
     * @param $array
     * @param int $parent
     * @return string
     */
    public function buildTree($array, $parent = 1)
    {
        $content = "";

        foreach ($array as $category) {
            if ($category[$this->parent] == $parent) {
                $content .= '<li>';
                $content .= CategoryItemTree::widget([
                    'id' => $category[$this->item],
                    'name' => $category[$this->name],
                    'url' => $this->getTagByUrl($category[$this->item]),
                    'classItem' => $category[$this->classItem] ?: '',
                    'tagContent' => $this->tagContent,
                ]);

                foreach ($array as $sub_category) {
                    if ($sub_category[$this->parent] == $category[$this->item]) {
                        $content .= Html::tag('ul', $this->buildTree($array, $category[$this->item]));
                        break;
                    }
                }
                $content .= '</li>';
            }
        }

        return $content;
    }

    /**
     * @param $category
     * @return string
     */
    public function getTagByUrl($category)
    {
        $content = "";
        $childrens = $this->getChildrens($category);

        if ($this->aWithChild) {
            $content = Url::to(['/category/' . $category]);
        } else {
            if (!$childrens) {
                $content = Url::to(['/category/' . $category]);
            }
        }

        return $content;
    }

    /**
     * @param $id
     * @return array
     */
    public function getChildrens($id)
    {
        $items = [];

        foreach ($this->items as $item) {
            if ($item[$this->parent] == $id) {
                $items[$item[$this->item]] = $this->getAttributes($item);
            }
        }

        return $items;
    }

    /**
     * @param Category $model
     * @return array
     */
    public function getAttributes($model)
    {
        return [
            'name' => $model[$this->name],
            'parent' => $model[$this->parent],
            'sort' => $model[$this->sort],
        ];
    }
}
