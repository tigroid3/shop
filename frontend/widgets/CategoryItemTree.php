<?php
namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class CategoryItemTree
 * @package frontend\widgets
 */
class CategoryItemTree extends Widget
{
    public $name;

    public $url;

    public $classItem;

    public $activeClass;

    public $tagContent;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('category-item-tree', [
            'id' => $this->id,
            'name' => $this->name,
            'url' => $this->url,
            'classItem' => $this->classItem,
            'activeClass' => $this->activeClass,
            'tagContent' => $this->tagContent,
        ]);
    }
}
