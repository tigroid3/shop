<?php
namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Class MainAsset
 * @package frontend\assets
 */
class MainAsset extends AssetBundle
{
    public $sourcePath = '@frontend/web/resources/theme/';

    public $css = [
        'main.css',
    ];

    public $js = [
        'main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
