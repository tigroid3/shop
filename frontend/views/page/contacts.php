<?php
use common\models\Lead;
use common\models\Page;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \frontend\models\CallbackForm $modelForm */
/** @var Page $page */

$this->title = Yii::t('common', 'Контакты');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Контакты')];
?>

<div class="p-contacts">
    <?= $page->content ?>
</div>
