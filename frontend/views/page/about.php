<?php
use common\models\Page;

/** @var Page $page */

?>

<div class="p-about">
    <?= $page->content ?>
</div>
