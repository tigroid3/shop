<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = Yii::t('common', Yii::t('common', 'Корзина'));
$this->params['breadcrumbs'][] = ['label' => $this->title];

/** @var \common\models\Item[] $positions */
/** @var \common\models\Item[] $randomItems */
?>

<div class="p-cart">
    <?php if ($positions): ?>
        <?php Pjax::begin([
            'id' => 'cart-positions-pjax'
        ]) ?>
        <table class="cart">
            <tbody>
            <?php foreach ($positions as $key => $position): ?>
                <tr data-item-id="<?= $position->id ?>">
                    <td class="check">
                        <input type="checkbox" data-item-id="<?= $position->id ?>" id="check-cart-<?= $position->id ?>">
                        <label for="check-cart-<?= $position->id ?>"></label>
                    </td>
                    <td class="img">
                        <?= Html::img(Url::toRoute('/image/' . (!is_null($position->defaultImage) ? $position->defaultImage->size_medium : "index"))) ?>
                    </td>
                    <td>
                        <p class="name"><?= $position->name ?></p>
                        <p class="article"><?= $position->code ? 'арт. ' . $position->code : "" ?></p>
                    </td>
                    <td class="count">
                        <span>Кол-во: </span>
                        <div class="number-input">
                            <input type="number" class="number-item" readonly="readonly"
                                   value="<?= Yii::$app->cart->getPositionById($position->id)->quantity ?>">
                            <a href="#" class="up-num"></a>
                            <a href="#" class="down-num"></a>
                        </div>
                    </td>
                    <td class="old-cost"><?= $position->discount ? $position->cost + ($position->cost * ($position->discount / 100)) . 'р.' : "" ?>
                    </td>
                    <td class="cost"><?= $position->cost * Yii::$app->cart->getPositionById($position->id)->quantity ?>
                        р.
                    </td>
                    <td class="discount"><?= $position->discount ? $position->discount . '%' : "" ?></td>
                    <td class="remove">
                        <?= Html::a('', Url::toRoute('/cart/delete/' . $position->id), ['class' => 'del-pos']) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php Pjax::end() ?>
        <div class="bottom-cart">
            <?= Html::a(Yii::t('common', 'Удалить выделенное'), '#', ['class' => 'del-checked']) ?>
            <?= Html::a(Yii::t('common', 'Оформить заказ'), Url::toRoute('/order/index'), [
                'class' => 'btn-customer new-order',
                'onClick' => 'yaCounter43756344.reachGoal ("order"); return true;',
            ]) ?>

            <p class="total">Общая сумма: <span class="sum"><?= Yii::$app->cart->getCost() ?> <span>р.</span></span></p>
        </div>
        <div class="assoc-goods">
            <h3 class="title">С этими товарами покупают</h3>
            <?php foreach ($randomItems as $randomItem): ?>
                <div class="item">
                    <div class="photo">
                        <?= Html::img(Url::toRoute('/image/' . (!is_null($randomItem->defaultImage) ? $randomItem->defaultImage->size_medium : ""), true)) ?>
                        <div class="block-buttons">
                            <?= Html::a('', Url::toRoute('/item/' . $randomItem->id), ['class' => 'view']) ?>
                            <?= Html::a('', Url::toRoute('#'), ['data-item-id' => $randomItem->id, 'class' => 'buy']) ?>
                        </div>
                    </div>
                    <p class="desc"><?= $randomItem->description ?></p>
                    <p class="cost"><?= $randomItem->cost ?> <span>р</span><span
                            class="discount"><?= $randomItem->discount ? '-' . $randomItem->discount . '%' : "" ?></span>
                    </p>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <p class="no-goods-in-cart"
       style="display:<?= $positions ? "none" : "block" ?>"><?= Yii::t('common', 'Нет добавленных товаров') ?></p>
</div>

