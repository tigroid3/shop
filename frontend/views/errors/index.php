<?php
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\web\HttpException $exception */
/** @var string $name */
/** @var string $message */

$this->title = $name;
?>

<div class="error-container container text-center">
    <div class="row" style="margin: 200px 0 0 0">
        <div class="col-xs-12">
            <i class="pe-7s-way text-info big-icon" style="font-size: 120px"></i>
            <h1 style="font-size: 60px"><?= $exception->statusCode ?></h1>

            <strong><?= Yii::t('common', 'При обработке вашего запроса произошла ошибка.') ?></strong>

            <p>
                <?= nl2br(Html::encode($message)) ?>
            </p>

            <a href="<?= Yii::$app->params['urlFrontend'] ?>"
               class="btn btn-xs btn-successn"
               style="font-size: 20px">
                <?= Yii::t('frontend', 'Вернуться на главную') ?>
            </a>

        </div>
    </div>
</div>
