<?php
use frontend\assets\module\CategoryAsset;
use yii\web\View;
use yii\widgets\LinkPager;
use yii\widgets\ListView;

/** @var yii\web\View $this */
/** @var \yii\data\ActiveDataProvider $dataProvider */
/** @var \yii\data\ActiveDataProvider $dataProviderCategory */
/** @var \common\models\search\ItemSearch $modelSearch */
/** @var \common\models\Category $category */
/** @var array $paramQuery */
/** @var integer $costFrom */
/** @var integer $costTo */
?>

<?php
$this->title = Yii::t('common', Yii::t('common', $category->name));
$this->params['breadcrumbs'][] = ['label' => $this->title];

$this->registerMetaTag(['name' => 'description', 'content' => $this->title]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->title]);

?>
<div class="catalog-block">
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'emptyText' => '',
        'itemOptions' => [
            'class' => 'col-sm-4',
        ],
        'layout' => '<div class="block-categories">{items}</div>'
    ])
    ?>

    <?php if ($dataProvider->getTotalCount() != 0): ?>
        <?= LinkPager::widget([
            'pagination' => $dataProvider->pagination,
        ]) ?>
    <?php endif; ?>
</div>
