<?php
use yii\helpers\Html;

/** @var \common\models\Item $model */
?>

<div class="product-image-wrapper">
    <div class="single-products">
        <div class="productinfo text-center">
            <div class="block-image">
                <?=Html::img() ?>
            </div>
            <img src="images/home/product1.jpg" alt="">
            <h2>₽ <?= $model->cost ?></h2>
            <p><?= $model->name ?></p>
            <?= Html::a('<i class="fa fa-shopping-cart"></i>В корзину', '#', [
                'class' => 'btn btn-default add-to-cart',
                'data-item-id' => $model->id
            ]) ?>
        </div>
    </div>
</div>
