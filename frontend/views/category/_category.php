<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \common\models\Category $model */

?>

<?= Html::a('', Url::toRoute('/category/' . $model->id)) ?>
<?= Html::img(Url::to('/image/' . $model->photo)) ?>
<p><?= $model->name ?></p>

