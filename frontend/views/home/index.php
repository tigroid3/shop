<?php
use common\models\Page;

/** @var Page $page */

$this->title = Yii::t('common', $page->title ? $page->title : Yii::t('common', 'Страница - ' . Yii::$app->name));
$this->registerMetaTag(['name' => 'description', 'content' => $page->meta_description ? $page->meta_description : $this->title . ' - ' . Yii::$app->name]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $page->meta_keywords ? $page->meta_keywords : $this->title . ' - ' . Yii::$app->name]);
?>

<?= $page->content ?>
