<?php
use yii\web\View;

/** @var \yii\web\View $this */

$notifiers = Yii::$app->notifier->getNotifiers();
?>

<?php if (!empty($notifiers) && is_array($notifiers)) : ?>
    <?php foreach ($notifiers as $notifier) : ?>
        <?= $this->registerJs("$.notify(
            {message:'" . $notifier["message"] . "'},
             {
                type: '" . $notifier['type'] . "',
                animate: {exit: 'hide'},
                z_index: 2050
             });", View::POS_END); ?>
    <?php endforeach; ?>
<?php endif; ?>
