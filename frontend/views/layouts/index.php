<?php
/** @var \yii\web\View $this */
/** @var string $content */

?>

<?php $this->beginContent('@frontend/views/layouts/common.php'); ?>
<?= $content ?>
<?php $this->endContent(); ?>
