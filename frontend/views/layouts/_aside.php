<?php
use common\models\Category;
use frontend\widgets\CategoryTree;

?>

<div class="col-sm-3">
    <div class="left-sidebar">
        <h2>Категории</h2>
        <?= CategoryTree::widget([
            'items' => Category::find()->all(),
            'classTree' => 'panel-group category-products'
        ]) ?>
        <div class="shipping text-center">
            <img src="images/home/shipping.jpg" alt=""/>
        </div>
    </div>
</div>
