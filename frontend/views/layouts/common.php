<?php
use frontend\assets\MainAsset;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var string $content */

MainAsset::register($this);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name=viewport content="width=1350">
    <meta name="google-site-verification" content="YlAfHkLpox9NEuuXmWLtyqIhXreYdAjet_gYKYB4ZJU"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> :: <?= Yii::$app->name ?></title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <?php $this->head(); ?>
</head>
<body>
<?php $this->beginBody(); ?>

<?= $this->render('_header') ?>

<section>
    <div class="container">
        <div class="row">
            <?= $this->render('_aside') ?>

            <div class="col-sm-9 padding-right">
                <?= $content ?>
            </div>
        </div>
    </div>
</section>


<?= $this->render('_footer') ?>

<?= $this->render('_notifiers') ?>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>

