<?php
use yii\helpers\Html;

$this->title = Yii::t('common', 'Поздравляем! Вы успешно оформили заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Завершение заказа')];

/** @var \common\models\Order $order */

?>

<div class="p-order-final">
    <div class="manager-callback">
        <?= Html::img('/resources/base/images/manager-callback.png', [
            'class' => 'img-manager'
        ]) ?>
        <div class="work-time-block">
            <p class="red">Режим работы</p>
            <p class="gr">Понедельник-Пятница</p>
            <p class="red">9:00 - 18:00</p>
            <p class="gr">Суббота - Воскресенье</p>
            <p class="red">ВЫХОДНОЙ</p>
        </div>

        <p class="message-callback-manager">В течении 15 минут оператор свяжется с вами для подтверждения заказа</p>
    </div>
    <div class="customer-info">
        <p><span><?= Yii::t('common', 'Номер заказа:') ?> </span><span class="high"><?= $order->id ?></span></p>
        <p><span><?= Yii::t('common', 'Сумма заказа:') ?> </span><span class="high"><?= $order->getTotalCostByModel() ?>
                р.</span></p>
        <!--        <p><span>Дата доставки: </span><spanclass="high">20 сентября 2017 с 12:00 до 16:00</span></p>-->
        <p><span>Адрес: </span><span class="high">
                <?= $order->customer_city ? $order->customer_city . ', ' : '' ?>
                <?= $order->customer_address ? 'ул. ' . $order->customer_address . ', ' : '' ?>
                <?= $order->number_home ? 'д.' . $order->number_home . ', ' : '' ?>
                <?= $order->number_flat ? 'кв.' . $order->number_flat . ', ' : '' ?>
            </span></p>
        <p><span>Получатель: </span><span class="high">
                <?= $order->surname ? $order->surname . ' ' : '' ?>
                <?= $order->customer_name ? $order->customer_name . ' ' : '' ?>
                <?= $order->patronymic ? $order->patronymic . ' ' : '' ?>
            </span></p>
        <table>
            <tbody>
            <?php foreach ($order->orderItems as $position): ?>
                <tr>
                    <td><?= Html::a($position->item->name, ['/item/' . $position->item_id], ['target' => '_blank']) ?></td>
                    <td class="article">арт. <?= $position->item->code ?></td>
                    <td><?= $position->quantity ?> <?= $position->item->unit ?></td>
                    <td class="cost"><?= $position->cost ?> р.</td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?= Html::a(Yii::t('common', 'Назад в магазин'), ['/category/1'], ['class' => "back-home"]) ?>
    </div>
</div>
