<?php
use common\models\Order;
use frontend\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \common\models\Order $order */

$this->title = Yii::t('common', 'Оформление заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Корзина'), 'url' => Url::toRoute('/cart')];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<div class="p-order-form">
    <div class="total-info">
        <p class="total-count"><?= Yii::t('common', 'Кол-во товаров:') ?>
            <span><?= Yii::$app->cart->getCount() ?></span></p>
        <p class="total-sum"><?= Yii::t('common', 'Общая сумма:') ?> <span><?= Yii::$app->cart->getCost() ?>р</span></p>
        <p class="total-weight"><?= Yii::t('common', 'Общий вес(без упаковки):') ?>
            <span><?= number_format($order->getTotalWeight(), 0, '.', ' ') . 'г' ?></span>
        </p>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'delivery-post',
        'action' => Url::toRoute('/order/index'),
        'enableAjaxValidation' => false
    ]) ?>
    <div class="select-delivery-block">
        <p>Выберите способ доставки: </p>
        <a href="#" data-select="2" class="select-a active-delivery"><?= Yii::t('common', 'Почтой России') ?></a>
        <a href="#" data-select="1" class="select-a"><?= Yii::t('common', 'Транспортной компанией') ?></a>

        <?= Html::activeHiddenInput($order, 'method', ['class' => 'method_delivery', 'value' => Order::DELIVERY_METHOD_POST]) ?>
    </div>
    <div class="wrapper-delivery-form">
        <div class="row-form row delivery-name">
            <p class="label-custom"><?= Yii::t('common', 'Выберите службу доставки:') ?></p>
            <div class="select2-custom-block">
                <?= $form->field($order, 'delivery')
                    ->select2List(Order::getAllDeliveries(), ['style' => 'width-250'])->label('') ?>
            </div>
        </div>
        <div class="row-form row russia-post-type-delivery">
            <p class="label-custom"><?= Yii::t('common', 'Выберите вид доставки:') ?></p>
            <div class="select2-custom-block">
                <?= $form->field($order, 'type_delivery')
                    ->select2List(Order::getTypesDeliveriesRussiaPost(), ['style' => 'width-250'])->label('') ?>
            </div>
        </div>
        <div class="row-form row russia-post-pay-method">
            <?php $order->pay_method = 2 ?>
            <?= Html::activeRadioList($order, 'pay_method', Order::getPaymentsMethod(), ['label' => '']) ?>

            <p class="info-pay info-electro-money">Оплату можно произвести только после подтверждения заказа
                оператором</p>
            <p class="info-pay info-cdo">Оплатить при получении посылки в почтовом отделении</p>
        </div>
        <div class="row-form radio-transport-type-delivery">
            <?php $order->typeDeliveryTransport = 5 ?>
            <?= Html::activeRadioList($order, 'typeDeliveryTransport', Order::getTypesDeliveriesTrasport(), ['label' => '']) ?>
        </div>
        <div class="row-form">
            <span>Получатель</span>
            <?= Html::activeInput('text', $order, 'surname', ['placeholder' => Yii::t('common', 'Фамилия')]) ?>
            <?= Html::activeInput('text', $order, 'customer_name', ['placeholder' => Yii::t('common', 'Имя')]) ?>
            <?= Html::activeInput('text', $order, 'patronymic', ['placeholder' => Yii::t('common', 'Отчество')]) ?>
        </div>
        <div class="row-form">
            <span>Адрес</span>
            <?= Html::activeInput('text', $order, 'customer_city', ['placeholder' => Yii::t('common', 'Город')]) ?>
            <?= Html::activeInput('text', $order, 'customer_address', ['placeholder' => Yii::t('common', 'Улица')]) ?>
            <?= Html::activeInput('text', $order, 'number_home', ['placeholder' => Yii::t('common', 'Дом')]) ?>
            <?= Html::activeInput('text', $order, 'number_flat', ['placeholder' => Yii::t('common', 'Кв')]) ?>
        </div>
        <div class="row-form">
            <span>Телефон</span>
            <?= Html::activeInput('text', $order, 'customer_phone', ['class' => 'phone-mask', 'placeholder' => Yii::t('common', '+7(950)123-4567')]) ?>
        </div>
        <div class="row-form">
            <span>Email</span>
            <?= Html::activeInput('text', $order, 'customer_email', ['placeholder' => Yii::t('common', 'example@mail.ru')]) ?>
        </div>
        <div class="row-form">
            <span>Комментарий</span>
            <?= Html::activeTextarea($order, 'comment', [
                'class' => 'comment-area',
                'rows' => 5,
                'placeholder' => Yii::t('common', 'Ваш коментарий...')
            ]) ?>
        </div>
        <div class="row-form buttons-block">
            <?= Html::a(Yii::t('common', 'Назад'), Url::toRoute('/cart'), ['class' => 'btn-customer back-to-cart']) ?>
            <?= Html::submitInput(Yii::t('common', 'Оформить заказ'), [
                'class' => 'text-right confirm-order'
            ]) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
