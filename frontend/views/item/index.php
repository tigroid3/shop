<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var \common\models\Item $model */

$this->title = Yii::t('common', $model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', Yii::t('common', $model->category->name)), 'url' => Url::toRoute('/category/' . $model->category->id)];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', $model->name)];

$this->registerMetaTag(['name' => 'description', 'content' => $this->title]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->title]);
?>

<div class="p-item">
    <div class="info-item">
        <div class="col-xs-5">
            <div class="flexslider">
                <ul class="slides">
                    <?php if ($model->video): ?>
                        <li>
                            <iframe width="<?= Yii::$app->params['imgSizes']['largeWidth'] ?>"
                                    height="<?= Yii::$app->params['imgSizes']['largeHeight'] ?>"
                                    src="<?= $model->getUrlYoutube() ?>" frameborder="0" allowfullscreen></iframe>
                        </li>
                    <?php endif; ?>

                    <?php if ($model->images): ?>
                        <?php foreach ($model->images as $image): ?>
                            <?= Html::tag('li',
                                Html::a(Html::img(Url::toRoute('/image/' . $image->size_large)),
                                    Url::toRoute('/image/' . $image->size_original),
                                    ['class' => 'fancybox', 'data-fancybox-group' => 'gallery']
                                )) ?>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <?= Html::tag('li',
                            Html::a(Html::img(Url::toRoute('/image/index/')))) ?>
                    <?php endif; ?>
                </ul>
            </div>
            <!--            <div>-->
            <!--                <div class="video-draw">-->
            <!--                    <div class="draw-img">-->
            <!--                        <img src="images/draw-img.png" alt="">-->
            <!--                    </div>-->
            <!--                    <a href="#" class="btn-customer download-draw">Скачать раскраску</a>-->
            <!--                </div>-->
            <!--            </div>-->
        </div>
        <div class="col-xs-7">
            <div class="full-info">
                <h3 class="title"><?= $model->name ?></h3>
                <p class="article">арт. <?= $model->code ?></p>
                <?php if ($model->height): ?>
                    <p>Высота: <?= $model->height . Yii::t('common', 'см') ?></p>
                <?php endif; ?>
                <?php if ($model->weight): ?>
                    <p>Вес: <?= $model->weight . Yii::t('common', 'г') ?></p>
                <?php endif; ?>
                <div class="desc">
                    <?= $model->description ?>
                </div>
                <div class="buttons-block">
                    <div class="cost-count">
                        <span>Кол-во:</span>
                        <div class="number-input">
                            <input type="text" class="number-item" readonly="readonly" value="1">
                            <a href="#" class="up-num"></a>
                            <a href="#" class="down-num"></a>
                        </div>
                        <div class="cost-block">
                        <span class="old-cost">
                            <? if ($model->discount && $model->cost) {
                                echo (number_format((($model->discount / 100 * $model->cost) + $model->cost), 0)) . 'р.';
                            } ?>
                        </span>
                            <span class="cost">
                            <?= $model->cost ? $model->cost . 'р.' : '-' ?>
                        </span>
                            <span class="discount">
                            <?= $model->discount ? '-' . $model->discount . '%' : "" ?>
                        </span>
                        </div>
                    </div>
                    <?= Html::a(Yii::t('common', 'Положить в корзину'), '#', [
                        'class' => 'btn-customer buy',
                        'data-item-id' => $model->id,
                        'onClick' => 'yaCounter43756344.reachGoal ("adInCartView"); return true;'
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>




