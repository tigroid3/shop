<?php
namespace frontend\models;

use common\models\Lead;
use Yii;
use yii\base\Model;

/**
 * Class Callback
 * @package frontend\models
 *
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $comment
 * @property string $type
 */
class CallbackForm extends Model
{
    public $name;
    public $phone;
    public $email;
    public $comment;
    public $type;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['comment', 'type'], 'string'],
            ['email', 'email'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Имя'),
            'phone' => Yii::t('common', 'Телефон'),
            'email' => Yii::t('common', 'Email'),
            'comment' => Yii::t('common', 'Комментарий'),
            'type' => Yii::t('common', 'Тип формы')
        ];
    }

    public function afterValidate()
    {
        $model = new Lead();
        $model->name = $this->name;
        $model->phone = $this->phone;
        $model->email = $this->email;
        $model->type = $this->type;
        $model->comment = $this->comment;
        $model->save();

        parent::afterValidate();
    }
}
