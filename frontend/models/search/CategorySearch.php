<?php
namespace frontend\models\search;

use common\models\Category;
use yii\data\ActiveDataProvider;

/**
 * Class CategorySearch
 * @package frontend\models\search
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param array $params
     * @param int $id
     * @return ActiveDataProvider
     */
    public function search($params = [], $id)
    {
        $query = Category::find()
            ->where([Category::tableName() . '.parent_id' => $id]);

        $config = [
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'active' => SORT_DESC,
                    'id' => SORT_ASC
                ]
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
