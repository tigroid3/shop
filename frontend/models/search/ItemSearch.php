<?php
namespace frontend\models\search;

use common\models\Category;
use common\models\Item;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

/**
 * Class ItemSearch
 * @package frontend\models\search
 */
class ItemSearch extends Item
{
    /** @var integer */
    public $page;

    /** @var integer */
    public $limit;

    /** @var string */
    public $name;

    /** @var integer */
    public $from;

    /** @var integer */
    public $to;

    /** @var  integer */
    public $sort;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'page', 'limit', 'from', 'to', 'sort'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @param $categoryId
     * @return ActiveDataProvider
     */
    public function search($params = [], $categoryId)
    {
        $query = Item::find()
            ->joinWith(['defaultImage', 'brand'])
            ->andWhere([Item::tableName() . '.show' => YES]);

        if ($categoryId != Category::ROOT_CATEGORY) {
            $query = $query->where(['category_id' => (int)$categoryId]);
        }

        $this->load($params);

        $config = [
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'active' => SORT_DESC,
                    'id' => SORT_ASC
                ]
            ]
        ];

        if (!$this->validate()) {
            return new ActiveDataProvider($config);
        }

        $query->andFilterWhere(['>=', Item::tableName() . '.cost', $this->from]);
        $query->andFilterWhere(['<=', Item::tableName() . '.cost', $this->to]);

        switch ($this->sort) {
            case 'sortCost':
                $query->orderBy(Item::tableName() . '.cost DESC');
                break;
            case 'sortCostR':
                $query->orderBy(Item::tableName() . '.cost');
                break;
            case 'sortCreate':
                $query->orderBy(Item::tableName() . '.created_at DESC');
                break;
            case 'sortCreateR':
                $query->orderBy(Item::tableName() . '.created_at');
                break;
        }

        $query->andFilterWhere(['LIKE', Item::tableName() . '.name', $this->name]);
        $query->orFilterWhere(['LIKE', Item::tableName() . '.code', $this->name]);

        $dataProvider = new ActiveDataProvider($config);

        return $dataProvider;
    }
}
