<?php
/**
 * Class Yii
 */
class Yii extends \yii\BaseYii
{
    /**
     * @var BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

/**
 * Class BaseApplication
 */
abstract class BaseApplication extends yii\base\Application
{

}

/**
 * Class WebApplication
 * @property \common\components\Notifier $notifier
 * @property \yz\shoppingcart\ShoppingCart $cart
 * @property \yii\swiftmailer\Mailer $mailer
 * @property \yii\image\ImageDriver $image
 * @property \yii\caching\FileCache $frontendCache
 * @property \yii\web\AssetManager $frontendAssetManager
 */
class WebApplication extends yii\web\Application
{

}

/**
 * Class ConsoleApplication
 */
class ConsoleApplication extends yii\console\Application
{

}
