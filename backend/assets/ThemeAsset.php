<?php
namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Class ThemeAsset
 * @package backend\assets
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/theme';

    public $css = [
        'nprogress.css',
        'custom.css',
    ];

    public $js = [
        'nprogress.js',
        'fastclick.js',
        'icheck.min.js',
        'bootstrap-progressbar.min.js',
        'custom.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'backend\assets\vendor\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'backend\assets\fonts\FontAwesomeAsset',
        'backend\assets\fonts\GlyphiconsAsset',
        'backend\assets\vendor\ToastrAsset',
    ];
}
