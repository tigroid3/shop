<?php
namespace backend\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class GlyphiconsAsset
 * @package backend\assets\fonts
 */
class GlyphiconsAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/fonts/glyphicons';

    public $css = [
        'glyphicons.css',
    ];
}
