<?php
namespace backend\assets\fonts;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package backend\assets\fonts
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/fonts/font-awesome';

    public $css = [
       'font-awesome.css',
    ];
}
