<?php
namespace backend\assets\widgets;

use yii\web\AssetBundle;

/**
 * Class ModalConfirmDeleteAsset
 * @package app\widgets\assets\custom
 */
class ModalConfirmDeleteAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@backend/web/resources/widgets/modal-confirm-delete';

    /**
     * @var array
     */
    public $js = [
        'modal-confirm-delete.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'backend\assets\base\ThemeAsset',
    ];
}
