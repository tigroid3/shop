<?php
namespace backend\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class BootstrapAsset
 * @package backend\assets\vendor
 */
class BootstrapAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/vendor/bootstrap';

    public $js = [
    ];

    public $css = [
        'bootstrap.min.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
