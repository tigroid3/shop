<?php
namespace backend\assets\vendor;

use yii\web\AssetBundle;

/**
 * Class ToastrAsset
 * @package backend\assets\vendor
 */
class ToastrAsset extends AssetBundle
{
    public $sourcePath = '@backend/web/resources/vendor/toastr';

    public $js = [
        'toastr.min.js',
        'toastr.custom.js',
    ];

    public $css = [
        'toastr.min.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
