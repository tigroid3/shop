<?php
namespace backend\models\search;

use common\models\OrderItem;
use yii\data\ActiveDataProvider;

/**
 * Class OrderItemSearch
 * @package backend\models\search
 */
class OrderItemSearch extends OrderItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['order_id', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = OrderItem::find()
            ->joinWith(['item']);

        $config = [
            'query' => $query,
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['order_id' => $this->order_id]);

        return $dataProvider;
    }
}
