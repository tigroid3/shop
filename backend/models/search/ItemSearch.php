<?php
namespace backend\models\search;

use common\models\Item;
use yii\data\ActiveDataProvider;

/**
 * Class ItemSearch
 * @package backend\models\search
 */
class ItemSearch extends Item
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Item::find()
            ->joinWith('category');

        $config = [
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['LIKE', Item::tableName() . '.name', $this->name]);
        $query->andFilterWhere(['LIKE', Item::tableName() . '.code', $this->code]);

        return $dataProvider;
    }
}
