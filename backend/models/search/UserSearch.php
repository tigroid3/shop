<?php
namespace backend\models\search;

use common\models\User;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * Class UserSearch
 * @package backend\models\search
 */
class UserSearch extends User
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Имя пользователя'),
            'role' => Yii::t('common', 'Роль'),
        ];
    }

    /**
     * @param array $params
     * @param bool $withPagination
     * @return ActiveDataProvider
     */
    public function search($params = [], $withPagination = true)
    {
        $sort = [];

        if (empty($params['sort'])) {
            $sort['id'] = SORT_DESC;
        }

        $query = User::find()
            ->where(['!=', '(status & ' . User::STATUS_DELETED . ')', User::STATUS_DELETED])
            ->orderBy($sort);

        $config = [
            'query' => $query,
        ];

        $config['pagination'] = [
            'pageSize' => $withPagination == true ? 20 : 0,
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }
}
