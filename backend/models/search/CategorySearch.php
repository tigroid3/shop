<?php
namespace backend\models\search;

use common\models\Category;
use yii\data\ActiveDataProvider;

/**
 * Class CategorySearch
 * @package backend\models\search
 */
class CategorySearch extends Category
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
            ['parent_id', 'integer']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Category::find()
            ->where(['!=', Category::tableName() . '.id', Category::ROOT_CATEGORY]);

        $config = [
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['LIKE', Category::tableName() . '.name', $this->name]);
        $query->andFilterWhere([Category::tableName() . '.parent_id' => $this->parent_id]);

        return $dataProvider;
    }
}
