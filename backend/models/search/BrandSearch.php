<?php
namespace backend\models\search;

use common\models\Brand;
use yii\data\ActiveDataProvider;

/**
 * Class BrandSearch
 * @package backend\models\search
 */
class BrandSearch extends Brand
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        $query = Brand::find();

        $config = [
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ];

        $dataProvider = new ActiveDataProvider($config);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['LIKE', 'name', $this->name]);

        return $dataProvider;
    }
}
