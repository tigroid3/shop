<?php
use yii\helpers\Html;

/** @var array $links */
?>

<?php if (!empty($links)): ?>
    <div class="btn-group dropdown-actions pull-right">
        <button class="btn btn-default btn-flat btn-sm dropdown-toggle" aria-expanded="false" type="button"
                data-toggle="dropdown"><?= Yii::t('common', 'Действия') ?> <span class="caret"></span>
        </button>

        <ul class="dropdown-menu dropdown-menu-right">
            <?php foreach ($links as $link): ?>
                <?php if (is_array($link)): ?>
                    <li>
                        <?= Html::a($link['label'], $link['url'], isset($link['attributes']) ? $link['attributes'] : []) ?>
                    </li>
                <?php else: ?>
                    <li class="divider"></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>
