<?php
/** @var string $label */
/** @var string $style */
/** @var string $size */
?>

<span class="label <?= $style ?> <?= $size ?>">
    <?= $label ?>
</span>
