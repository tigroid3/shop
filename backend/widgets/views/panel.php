<?php
/** @var string $id */
/** @var string $title */
/** @var string $content */
/** @var string $footerContent */
?>

<div class="x_panel" <?php if ($id): ?> id="<?= $id ?>" <?php endif; ?>>
    <div class="x_title">
        <h2><?= $title ?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?= $content ?>
    </div>
    <div class="x_content x_panel_footer">
        <?= $footerContent ?>
    </div>
</div>
