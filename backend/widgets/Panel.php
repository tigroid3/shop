<?php
namespace backend\widgets;
use yii\base\Widget;

/**
 * Class Panel
 * @package backend\widgets
 */
class Panel extends Widget
{
    /** @var string */
    public $id;

    /** @var string */
    public $title;

    /** @var string */
    public $content;

    /** @var string */
    public $footerContent;

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('panel', [
            'id' => $this->id,
            'title' => $this->title,
            'content' => $this->content,
            'footerContent' => $this->footerContent,
        ]);
    }
}
