<?php
namespace backend\widgets;

use yii\base\Widget;

/**
 * Class DropdownActions
 * @package backend\widgets
 */
class DropdownActions extends Widget
{
    const SIZE_LARGE = 'btn-lg';
    const SIZE_SMALL = 'btn-sm';
    const SIZE_TINY = 'btn-xs';

    /**
     * @var array Ссылки
     */
    public $links = [];

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('dropdown-actions', [
            'links' => $this->links
        ]);
    }
}
