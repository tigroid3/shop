<?php
namespace backend\modules\content;

/**
 * Class Module
 * @package backend\modules\content
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\content\controllers';
}
