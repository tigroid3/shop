<?php
use backend\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \common\models\Article $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление статьи') : Yii::t('common', 'Редактирование статьи');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Конент'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Статьи'), 'url' => Url::toRoute('/content/article')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php Pjax::begin() ?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Статья'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>
<?php Pjax::end() ?>
