<?php
use backend\components\grid\ActionColumn;
use yii\grid\GridView;
use backend\models\search\NewsSearch;
use backend\widgets\Panel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var NewsSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список новостей');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Контент'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список новостей'),
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'title',
            'meta_title',
            'meta_description',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d-m-Y H:i']
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить новость'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footerContent' => Html::a(Yii::t('common', 'Добавить новость'), ['news/edit'], ['class' => 'btn btn-success pull-left']) .
        LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'options' => [
                'class' => 'pagination pull-right m-0'
            ]
        ])
]) ?>

<?//= ModalConfirmDelete::widget(); ?>
