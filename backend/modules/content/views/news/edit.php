<?php
use backend\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \common\models\News $model */
/** @var \common\models\form\UploadImageForm $imageModel */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление новости') : Yii::t('common', 'Редактирование новости');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Контент'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Новости'), 'url' => Url::toRoute('/content/news/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php Pjax::begin() ?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Новость'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'imageModel' => $imageModel
    ])
]); ?>
<?php Pjax::end() ?>
