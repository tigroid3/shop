<?php
use common\helpers\Image;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \common\models\News $model */
/** @var \common\models\form\UploadImageForm $imageModel */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => ['enctype' => 'multipart/form-data']
]); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'title')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'meta_title')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'meta_description')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'meta_keywords')->textInput() ?>
    </div>
</div>
<div class="row m-b-md">
    <div class="col-sm-12">
        <?= $form->field($model, 'short_content')->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
                'allowedContent' => true
            ])
        ]) ?>
    </div>
</div>
<div class="row m-b-md">
    <div class="col-sm-12">
        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
                'allowedContent' => true
            ])
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <?= $form->field($imageModel, 'files[]')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => false,
                'showUpload' => false,
                'allowedExtensions' => ['jpg', 'jpeg', 'png'],
            ]
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 m-b-lg">
        <?= Html::img(Image::getUrlImage($model->photo, Image::SUB_DIR_NEWS)) ?>
    </div>
</div>
<div class="row m-t-md">
    <div class="col-sm-12">
        <?= Html::submitInput(Yii::t('common', $model->isNewRecord ? 'Добавить новость' : 'Сохранить новость'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
