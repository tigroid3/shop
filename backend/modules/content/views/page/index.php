<?php
use backend\components\grid\ActionColumn;
use backend\models\search\PageSearch;
use backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var PageSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список страниц');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Контент'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_index-filter', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список страниц'),
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d-m-Y H:i']
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить cтраницу'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footerContent' => Html::a(Yii::t('common', 'Добавить страницу'), ['page/edit'], ['class' => 'btn btn-success pull-left']) .
        LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'options' => [
                'class' => 'pagination pull-right m-0'
            ]
        ])
]) ?>

<? //= ModalConfirmDelete::widget(); ?>
