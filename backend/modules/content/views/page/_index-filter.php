<?php
use backend\models\search\PageSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var PageSearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($modelSearch, 'title')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= Html::submitInput(Yii::t('common', 'Применить'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Сбросить фильтр'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
