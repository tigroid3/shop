<?php
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \common\models\Page $model */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
]); ?>
<div class="row">
    <div class="col-sm-4">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-sm-8">
        <?= $form->field($model, 'title')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'meta_title')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'meta_description')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'meta_keywords')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?= $form->field($model, 'content')->widget(CKEditor::className(), [
            'editorOptions' => ElFinder::ckeditorOptions(['elfinder'], [
                'allowedContent' => true
            ])
        ]) ?>
    </div>
</div>
<div class="row m-t-md">
    <div class="col-lg-12">
        <?= Html::submitInput(Yii::t('common', $model->isNewRecord ? 'Добавить страницу' : 'Сохранить страницу'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
