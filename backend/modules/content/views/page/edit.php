<?php
use backend\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \common\models\Page $model */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление страницы') : Yii::t('common', 'Редактирование страницы');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Конент'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Страницы'), 'url' => Url::toRoute('/content/page')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php Pjax::begin() ?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Страница'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>
<?php Pjax::end() ?>
