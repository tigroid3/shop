<?php
namespace backend\modules\content\controllers;

use backend\components\Controller;
use common\models\Page;
use backend\models\search\PageSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class PageController
 * @package backend\modules\content\controllers
 */
class PageController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new PageSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Page();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->notifier->addNotifier($model->isNewRecord ? Yii::t('common', 'Страница успешно добавлена.') : Yii::t('common', 'Страница успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Страница успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Page
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Page::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Страница не найдена.'));
        }

        return $model;
    }
}
