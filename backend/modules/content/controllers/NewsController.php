<?php
namespace backend\modules\content\controllers;

use backend\components\Controller;
use common\models\form\UploadImageForm;
use common\models\News;
use backend\models\search\NewsSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class NewsController
 * @package backend\modules\content\controllers
 */
class NewsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new NewsSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new News();
        }

        $imageModel = new UploadImageForm();
        $imageModel->files = UploadedFile::getInstances($imageModel, 'files');

        if ($imageModel->files) {
            $model->uploadImage($imageModel->files);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->notifier->addNotifier($model->isNewRecord ? Yii::t('common', 'Новость успешно добавлена.') : Yii::t('common', 'Новость успешно отредактирована.'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'imageModel' => $imageModel
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Страница успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return News
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = News::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Страница не найдена.'));
        }

        return $model;
    }
}
