<?php
namespace backend\modules\content\controllers;

use backend\components\Controller;
use common\models\Article;
use backend\models\search\ArticleSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class ArticleController
 * @package backend\modules\content\controllers
 */
class ArticleController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ArticleSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Article();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->notifier->addNotifier($model->isNewRecord ? Yii::t('common', 'Статья успешно добавлена.') : Yii::t('common', 'Статья успешно отредактирована.'), 'success');

                return $this->redirect(Url::toRoute('index'));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Статья успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Article
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Article::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Страница не найдена.'));
        }

        return $model;
    }
}
