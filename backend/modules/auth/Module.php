<?php
namespace backend\modules\auth;

/**
 * Class Module
 * @package backend\modules\auth
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\auth\controllers';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }
}
