<?php
use backend\assets\ThemeAsset;
use yii\helpers\Html;
use yii\web\View;

/** @var string $content */
/** @var View $this */

ThemeAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head class="nav-md">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<?php $this->beginBody() ?>
<body class="login">
<div class="login_wrapper">
    <?= $content ?>
</div>
<?php $this->endBody() ?>
<?= $this->render('@backend/views/common/_notifiers') ?>
</body>
</html>
<?php $this->endPage() ?>
