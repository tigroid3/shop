<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \yii\web\View $this */
/** @var \backend\modules\auth\models\LoginForm $model */

$this->title = Yii::t('common', 'Авторизация');
?>

<div class="animate form login_form">
    <section class="login_content">
        <?php $form = ActiveForm::begin([
            'action' => Url::to('/auth/login'),
            'enableClientValidation' => false
        ]) ?>

        <h1><?= Yii::t('common', 'Авторизация') ?></h1>
        <div>
            <?= $form->field($model, 'username')->textInput([
                'required' => "",
                'placeholder' => $model->getAttributeLabel('username')
            ])?>
        </div>
        <div>
            <?= $form->field($model, 'password')->passwordInput([
                'required' => "",
                'placeholder' => $model->getAttributeLabel('password')
            ])?>
        </div>
        <div>
            <?= Html::submitInput(Yii::t('common', 'Войти'), ['class' => 'btn btn-primary']) ?>
        </div>

        <div class="clearfix"></div>

        <div class="separator">
            <br/>
            <div>
                <h1><i class="fa fa-paw"></i> <?= Yii::t('common', 'Админ панель') ?></h1>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </section>
</div>
