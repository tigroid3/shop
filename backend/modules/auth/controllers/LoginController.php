<?php
namespace backend\modules\auth\controllers;

use backend\modules\auth\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Class LoginController
 * @package backend\modules\auth\controllers
 */
class LoginController extends Controller
{
    public $layout = 'login';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionIndex()
    {
        $model = new LoginForm();

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());

            if ($model->login()) {
                return $this->goBack();
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
