<?php
namespace backend\modules\auth\models;

use common\models\User;
use Yii;
use yii\base\Model;

/**
 * Class LoginForm
 * @package backend\modules\auth\models
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = false;

    /**
     * @var boolean|User
     */
    private $user = false;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['rememberMe', 'boolean'],
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('common', 'Имя пользователя'),
            'password' => Yii::t('common', 'Пароль'),
            'rememberMe' => Yii::t('common', 'Запомнить'),
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, Yii::t('common', 'Неверное имя пользователя или пароль.'));
            }
        }
    }

    /**
     * @return null|User
     */
    public function getUser()
    {
        if ($this->user === false) {
            $this->user = User::find()->where(['username' => $this->username])->one();
        }

        return $this->user;
    }

    /**
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }
}
