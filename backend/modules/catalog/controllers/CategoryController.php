<?php
namespace backend\modules\catalog\controllers;

use backend\components\Controller;
use backend\models\search\CategorySearch;
use common\models\Category;
use common\models\form\UploadImageForm;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class CategoryController
 * @package backend\modules\catalog\controllers
 */
class CategoryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new CategorySearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Category();
        }

        $imageModel = new UploadImageForm();
        $imageModel->files = UploadedFile::getInstances($imageModel, 'files');

        if ($imageModel->validate()) {
            $model->uploadImage($imageModel->files);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', $model->isNewRecord ? 'Категория успешно добавлена' : 'Категория успешно отредактирована'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'imageModel' => $imageModel
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionActivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 1;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Категория успешно активирована'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeactivate($id)
    {
        $model = $this->getModel($id);

        $model->active = 0;

        if ($model->save(true, ['active'])) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Категория успешно деактивирована'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->id !== Category::ROOT_CATEGORY) {
            if ($model->delete()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Категория успешно удалена'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Нелья удалить корневой каталог'), 'success');
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Category
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Category::find()
            ->where([
                Category::tableName() . '.id' => $id
            ])->one();

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Страна не найдена'));
        }

        return $model;
    }
}
