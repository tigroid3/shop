<?php
namespace backend\modules\catalog\controllers;

use backend\components\Controller;
use backend\models\search\ItemSearch;
use common\models\Item;
use common\models\ItemImage;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class ItemController
 * @package backend\modules\catalog\controllers
 */
class ItemController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new ItemSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new Item();
        }

        if ($model->load(Yii::$app->request->post())) {
            $isNewRecord = $model->isNewRecord;

            if ($model->save()) {
                $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
                $model->uploadImages();

                Yii::$app->notifier->addNotifier($isNewRecord ? Yii::t('common', 'Товар успешно добавлен.') : Yii::t('common', 'Товар успешно отредактирован.'), 'success');

                return $this->redirect(Url::toRoute(['item/edit', 'id' => $model->id]));

            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Товар успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionPhotoDelete($id)
    {
        $model = ItemImage::find()
            ->where(['id' => $id])
            ->one();

        if ($model) {
            $itemId = $model->item_id;

            if ($model->delete()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Фото успешно удалено.'), 'success');

                return $this->redirect(Url::to(['edit', 'id' => $itemId]));
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDefault($id)
    {
        $model = ItemImage::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Фото не найдено.'));
        }

        if ($model->default == 1) {
            $model->default = 0;
        } else {
            ItemImage::updateAll(['default' => 0], ['item_id' => $model->item_id]);
            $model->default = 1;
        }

        $model->save();

        return $this->redirect(Url::toRoute(['item/edit', 'id' => $model->item_id . '?' . uniqid()]));
    }

    /**
     * @param $id
     * @return Item
     * @throws HttpException
     */
    private function findModel($id)
    {
        $model = Item::find()
            ->joinWith(['images', 'brand'])
            ->where([Item::tableName() . '.id' => $id])
            ->one();

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Товар не найден.'));
        }

        return $model;
    }
}
