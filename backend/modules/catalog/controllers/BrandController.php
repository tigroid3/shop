<?php
namespace backend\modules\catalog\controllers;

use backend\components\Controller;
use backend\models\search\BrandSearch;
use common\models\Brand;
use common\models\form\UploadImageForm;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;
use yii\web\UploadedFile;

/**
 * Class BrandController
 * @package backend\modules\catalog\controllers
 */
class BrandController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new BrandSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'modelSearch' => $modelSearch,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param null $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        if ($id) {
            $model = $this->getModel($id);
        } else {
            $model = new Brand();
        }

        $imageModel = new UploadImageForm();
        $imageModel->files = UploadedFile::getInstances($imageModel, 'files');

        if ($imageModel->validate()) {
            $model->uploadImage($imageModel->files);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->notifier->addNotifier($model->isNewRecord ? Yii::t('common', 'Бренд успешно добавлен.') : Yii::t('common', 'Бренд успешно отредактирован.'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }
        }

        return $this->render('edit', [
            'model' => $model,
            'imageModel' => $imageModel
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws \yii\web\HttpException
     */
    public function actionDelete($id)
    {
        $model = $this->getModel($id);

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Бренд успешно удален.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute('index'));
    }

    /**
     * @param integer $id
     * @return Brand
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Brand::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Бренд не найден.'));
        }

        return $model;
    }
}
