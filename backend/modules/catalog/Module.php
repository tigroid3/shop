<?php
namespace backend\modules\catalog;

/**
 * Class Module
 * @package backend\modules\catalog
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\catalog\controllers';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

}
