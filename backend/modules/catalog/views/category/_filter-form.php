<?php
use common\models\Category;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \backend\models\search\CategorySearch $modelSearch */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index'),
    'method' => 'get',
    'enableClientValidation' => false,
]); ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'name')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($modelSearch, 'parent_id')->widget(Select2::className(), [
            'data' => Category::find()->collection()
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= Html::submitInput(Yii::t('common', 'Применить'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Сбросить фильтр'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
