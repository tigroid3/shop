<?php
use backend\components\grid\ActionColumn;
use backend\widgets\Label;
use backend\widgets\Panel;
use common\helpers\Image;
use common\models\Category;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var array $categories */
/** @var \backend\models\search\CategorySearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */
/** @var yii\web\View $this */

$this->title = Yii::t('common', 'Список категорий');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch
    ])
])
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список категорий'),
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{summary}{pager}{items}",
        'columns' => [
            'id',
            'name',
            [
                'attribute' => 'parent.name',
                'label' => Yii::t('common', 'Родительская категория')
            ],
            'alias',
            [
                'attribute' => 'activate',
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'content' => function (Category $model) {
                    if ($model->active) {
                        return Label::widget([
                            'label' => Yii::t('common', 'Активная'),
                            'style' => Label::STYLE_SUCCESS
                        ]);
                    } else {
                        return Label::widget([
                            'label' => Yii::t('common', 'Деактивирована'),
                        ]);
                    }
                }
            ],
            [
                'attribute' => 'photo',
                'contentOptions' => [
                    'class' => 'text-center admin-list-photo'
                ],
                'content' => function (Category $model) {
                    return Html::img(Image::getUrlImage($model->photo, Image::SUB_DIR_CATEGORIES));
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d-m-Y H:i']
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Показать'),
                        'url' => function ($model) {
                            return Url::toRoute(['deactivate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return $model->active ? true : false;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Скрыть'),
                        'url' => function ($model) {
                            return Url::toRoute(['activate', 'id' => $model->id]);
                        },
                        'can' => function ($model) {
                            return $model->active ? false : true;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($model) {
                            return Url::toRoute(['delete', 'id' => $model->id]);
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footerContent' => Html::a(Yii::t('common', 'Добавить категорию'), ['category/edit'], ['class' => 'btn btn-success pull-left']) .
        LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'options' => [
                'class' => 'pagination pull-right m-0'
            ]
        ])
]) ?>

<? //= ModalConfirmDelete::widget(); ?>
