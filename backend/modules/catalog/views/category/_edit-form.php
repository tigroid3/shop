<?php
use common\helpers\Image;
use common\models\Category;
use common\models\form\UploadImageForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \common\models\Category $model */
/** @var UploadImageForm $imageModel */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]); ?>
<?php if ($model->id !== Category::ROOT_CATEGORY): ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'parent_id')->widget(Select2::className(), [
                'data' => $model->getParentsCategories($model->id),
                'options' => [
                    'prompt' => '—',
                ]
            ]) ?>
        </div>
        <div class="col-lg-2">
            <?= $form->field($model, 'position')->textInput() ?>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($imageModel, 'files[]')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => false,
                'showUpload' => false,
                'allowedExtensions' => ['jpg', 'jpeg', 'png'],
            ]
        ]) ?>
    </div>
    <div class="col-lg-3">
        <?= $form->field($model, 'alias')->textInput([
            'disabled' => 'disabled'
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 m-b-lg">
        <?= Html::img(Image::getUrlImage($model->photo, Image::SUB_DIR_CATEGORIES)) ?>
    </div>
</div>
<div class="row m-t-md">
    <div class="col-lg-12">
        <?= Html::submitInput(Yii::t('common', $model->isNewRecord ? 'Добавить категорию' : 'Сохранить категорию'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

