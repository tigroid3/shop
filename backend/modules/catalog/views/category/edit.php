<?php
use backend\widgets\Panel;
use common\models\form\UploadImageForm;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \common\models\Category $model */
/** @var UploadImageForm $imageModel */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление категории') : Yii::t('common', 'Редактирование категории');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/category/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php Pjax::begin() ?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Категория'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'imageModel' => $imageModel,
    ])
]); ?>
<?php Pjax::end() ?>
