<?php
use common\helpers\Image;
use common\models\form\UploadImageForm;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \common\models\Brand $model */
/** @var UploadImageForm $imageModel */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]); ?>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($imageModel, 'files[]')->widget(FileInput::className(), [
            'pluginOptions' => [
                'showPreview' => false,
                'showUpload' => false,
                'allowedExtensions' => ['jpg', 'jpeg', 'png'],
            ]
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4 m-b-lg">
        <?= Html::img(Image::getUrlImage($model->photo, Image::SUB_DIR_BRANDS)) ?>
    </div>
</div>
<div class="row m-t-md">
    <div class="col-lg-12">
        <?= Html::submitInput(Yii::t('common', $model->isNewRecord ? 'Добавить бренд' : 'Сохранить бренд'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
