<?php
use backend\components\grid\ActionColumn;
use backend\models\search\BrandSearch;
use backend\widgets\Panel;
use common\helpers\Image;
use common\models\Brand;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var BrandSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список брендов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];

?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch
    ])
])
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список брендов'),
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{summary}{pager}{items}",
        'columns' => [
            'id',
            'name',
            'alias',
            [
                'attribute' => 'photo',
                'contentOptions' => [
                    'class' => 'text-center admin-list-photo'
                ],
                'content' => function (Brand $model) {
                    return Html::img(Image::getUrlImage($model->photo, Image::SUB_DIR_BRANDS));
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d-m-Y H:i']
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($model) {
                            return Url::toRoute(['delete', 'id' => $model->id]);
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footerContent' => Html::a(Yii::t('common', 'Добавить бренд'), ['brand/edit'], ['class' => 'btn btn-success pull-left']) .
        LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'options' => [
                'class' => 'pagination pull-right m-0'
            ]
        ])
]) ?>

<? //= ModalConfirmDelete::widget(); ?>
