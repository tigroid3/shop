<?php
use backend\widgets\Panel;
use common\models\form\UploadImageForm;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \common\models\Brand $model */
/** @var UploadImageForm $imageModel */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление бренда') : Yii::t('common', 'Редактирование бренда');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Бренды'), 'url' => Url::toRoute('/catalog/brand/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php Pjax::begin() ?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Бренд'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
        'imageModel' => $imageModel
    ])
]); ?>
<?php Pjax::end() ?>
