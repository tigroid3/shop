<?php
use backend\widgets\Panel;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \common\models\Item $model */
/** @var \common\models\form\UploadImageForm $imageModel */

$this->title = $model->isNewRecord ? Yii::t('common', 'Добавление товара') : Yii::t('common', 'Редактирование товара');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Товары'), 'url' => Url::toRoute('/catalog/item/index')];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>
<?php Pjax::begin() ?>
<?= Panel::widget([
    'title' => $this->title,
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
])
?>
<?php Pjax::end() ?>
