<?php
use common\helpers\Image;
use common\models\Brand;
use common\models\Category;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \common\models\Item $model */

?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id]),
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]); ?>
<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'name')->textInput() ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'category_id')->widget(Select2::className(), [
            'data' => Category::find()->collection()
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'video')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'brand_id')->widget(Select2::className(), [
            'data' => Brand::find()->collection(),
            'options' => [
                'prompt' => '—',
            ]
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'code')->textInput(['disabled' => 'true']) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <?= $form->field($model, 'cost')->textInput() ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'discount')->textInput([
            'placeholder' => '%'
        ]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'reserve')->textInput() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= Html::label($model->getAttributeLabel('photo')) ?>
        <?= FileInput::widget([
            'model' => $model,
            'attribute' => 'imageFiles[]',
            'options' => [
                'class' => 'form-group',
                'multiple' => true,
            ],
            'pluginOptions' => [
                'allowedExtensions' => ['jpg', 'jpeg', 'png'],
            ]
        ]);
        ?>
    </div>
</div>
<div class="row m-t-md">
    <div class="col-lg-12">
        <?= Html::submitInput(Yii::t('common', $model->isNewRecord ? 'Добавить позицию' : 'Сохранить позицию'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>

<div class="row m-t-md">
    <?php foreach ($model->images as $image): ?>
        <div class="col-sm-2 text-center img-item">
            <?= Html::img(Image::getUrlImage($image->size_medium, Image::SUB_DIR_ITEMS)) ?>

            <?= Html::a(Yii::t('common', 'Сделать главной'), ['default', 'id' => $image->id], ['class' => 'btn btn-info full-width m-t-sm']) ?>
            <?= Html::a(Yii::t('common', 'Удалить'), ['photo-delete', 'id' => $image->id], ['class' => 'btn btn-danger full-width']) ?>
        </div>
    <?php endforeach; ?>
</div>
<?php ActiveForm::end(); ?>
