<?php
use backend\components\grid\ActionColumn;
use backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var \backend\models\search\ItemSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список товаров');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Справочники'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Фильтр поиска'),
    'content' => $this->render('_filter-form', [
        'modelSearch' => $modelSearch,
    ]),
]) ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список товаров'),
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{summary}{pager}{items}",
        'columns' => [
            'id',
            'name',
            'category.name',
            'alias',
            'code',
            'cost',
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                        'can' => function () {
                            return Yii::$app->user->can('catalog.product.edit');
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить товар'),
                        'url' => function () {
                            return '#';
                        },
                        'style' => 'confirm-delete-link',
                        'attributes' => function ($model) {
                            return [
                                'data-href' => Url::toRoute(['delete', 'id' => $model->id]),
                            ];
                        },
                        'can' => function () {
                            return Yii::$app->user->can('access.role.delete');
                        },
                    ],
                ]
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Редактировать'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Скрыть'),
                        'url' => function  ($model) {
                            return Url::toRoute(['show', 'id' => $model->id]);
                        },
                        'can' => function  ($model) {
                            return $model->show ? true : false;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Показать'),
                        'url' => function  ($model) {
                            return Url::toRoute(['show', 'id' => $model->id]);
                        },
                        'can' => function  ($model) {
                            return $model->show ? false : true;
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'Удалить'),
                        'url' => function ($model) {
                            return Url::toRoute(['delete', 'id' => $model->id]);
                        },
                    ],
                ]
            ]
        ],
    ]),
    'footerContent' => Html::a(Yii::t('common', 'Добавить товар'), ['item/edit'], ['class' => 'btn btn-success pull-left']) .
        LinkPager::widget([
            'pagination' => $dataProvider->pagination,
            'options' => [
                'class' => 'pagination pull-right m-0'
            ]
        ])
]) ?>
