<?php
use backend\components\grid\ActionColumn;
use backend\modules\administration\components\runtime\Log;
use backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \yii\web\View $this */
/** @var \yii\data\ArrayDataProvider $dataProvider */

$this->title = Yii::t('common', 'Логирование');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Администрирование'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Логирование')];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список логов'),
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'label' => Yii::t('common', 'Лог'),
                'enableSorting' => false,
                'content' => function (Log $model) {
                    return $model->name . ' - ' . Html::tag('span', $model->fileName, ['class' => 'text-gray']);
                },
            ],
            [
                'label' => Yii::t('common', 'Счетчики'),
                'enableSorting' => false,
                'contentOptions' => [
                    'class' => 'text-center'
                ],
                'content' => function (Log $model) {
                    return $this->render('_index-counter', ['log' => $model]);
                }
            ],
            [
                'attribute' => 'size',
                'label' => Yii::t('common', 'Размер'),
                'enableSorting' => false,
                'format' => 'shortSize',
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
            ],
            [
                'attribute' => 'updatedAt',
                'format' => ['date', 'php:d-m-Y H:i'],
                'label' => Yii::t('common', 'Дата изменения'),
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'enableSorting' => false,
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Просмотреть'),
                        'url' => function (Log $model) {
                            return Url::toRoute(['view', 'name' => urlencode($model->name)]);
                        },
                        'attributes' => function () {
                            return [
                                'target' => '_blank',
                            ];
                        },
                    ],
                    [
                        'label' => Yii::t('common', 'История'),
                        'url' => function (Log $model) {
                            return Url::toRoute(['history', 'name' => urlencode($model->name)]);
                        }
                    ],
                    [
                        'label' => Yii::t('common', 'Архивировать'),
                        'url' => function (Log $model) {
                            return Url::toRoute(['archive', 'name' => urlencode($model->name)]);
                        }
                    ],
                ]
            ]
        ],
    ]),
]) ?>
