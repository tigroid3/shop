<?php
use backend\modules\administration\components\runtime\Log;
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \backend\modules\administration\components\runtime\Log $log */

foreach ($log->getCounts() as $level => $count) {
    echo Html::tag('span', $count, [
        'class' => 'label ' . Log::getLevelClass($level),
        'title' => $level,
    ]);
    echo ' ';
}
