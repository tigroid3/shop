<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\widgets\Panel;
use yii\helpers\Url;

/** @var \common\models\Options $model */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute('index')
]); ?>
<?= Panel::widget([
    'title' => Yii::t('common', 'Список параметров'),
    'content' => $this->render('fields', [
        'model' => $model,
        'form' => $form
    ]),
    'footerContent' => Html::submitInput(Yii::t('common', 'Сохранить данные'), [
        'class' => 'btn btn-primary btn-sm'
    ])
]) ?>
<?php ActiveForm::end(); ?>
