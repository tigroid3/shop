<?php
use common\models\Options;
use yii\widgets\ActiveForm;

/** @var ActiveForm $form */
/** @var \common\models\Options $model */

?>

<div class="row">
    <div class="col-xs-4">
        <?= $form->field($model, 'emails')->textarea([
            'rows' => 5,
            'value' => implode("\n", Options::getEmails()),
        ]) ?>
    </div>
</div>
