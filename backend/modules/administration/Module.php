<?php
namespace backend\modules\administration;

use yii\base\Module as ModuleBase;

/**
 * Class Module
 * @package backend\modules\administration
 */
class Module extends ModuleBase
{
    public $controllerNamespace = 'backend\modules\administration\controllers';
}
