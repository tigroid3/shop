<?php
namespace backend\modules\administration\controllers;

use backend\components\Controller;
use common\components\Notifier;
use common\components\UploaderCsv;
use Yii;

/**
 * Class UploaderController
 * @package backend\modules\administration\controllers
 */
class UploaderController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $uploader = new UploaderCsv();
        $uploader->upload();

        Yii::$app->notifier->addNotifier(Yii::t('common', 'Выгрузка успешно завершена'), Notifier::TYPE_SUCCESS);

        $this->redirect('/');
    }
}
