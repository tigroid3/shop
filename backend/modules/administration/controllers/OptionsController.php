<?php
namespace backend\modules\administration\controllers;

use backend\components\Controller;
use common\components\Notifier;
use common\models\Options;
use Yii;

/**
 * Class OptionsController
 * @package backend\modules\administration\controllers
 */
class OptionsController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = new Options();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $emails = @explode("\n", $model->emails);

                if ($emails) {
                    $this->saveData('emails', Yii::t('common', 'Почта для рассылки'), serialize($emails));

                    Yii::$app->notifier->addNotifier(Yii::t('common', 'Успешно сохранено'), Notifier::TYPE_SUCCESS);
                }
            }
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * @param $alias
     * @param $name
     * @param $value
     */
    public function saveData($alias, $name, $value)
    {
        $model = Options::find()->byAlias($alias)->one();

        if (!$model) {
            $model = new Options();
        }

        $model->alias = $alias;
        $model->name = $name;
        $model->value = $value;
        $model->save();
    }
}
