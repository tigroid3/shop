<?php
namespace backend\modules\administration\components\runtime;

use Yii;
use yii\base\Component;
use yii\helpers\FileHelper;
use yii\caching\FileDependency;

/**
 * Class Log
 * @package backend\modules\administration\components\runtime
 */
class Log extends Component
{
    /**
     * @var string
     */
    public $name = '';

    /**
     * @var string
     */
    public $alias = '';

    /**
     * @var string
     */
    public $fileName = '';

    /**
     * @param string $alias
     * @param string|null $stamp
     * @return string
     */
    public static function extractFileName($alias, $stamp = null)
    {
        $fileName = FileHelper::normalizePath(Yii::getAlias($alias, false));

        if ($stamp === null) {
            return $fileName;
        }

        $info = pathinfo($fileName);

        return strtr('{dir}/{name}.{stamp}.{ext}', [
            '{dir}' => $info['dirname'],
            '{name}' => $info['filename'],
            '{stamp}' => $stamp,
            '{ext}' => $info['extension'],
        ]);
    }

    /**
     * @param string $alias
     * @param string $fileName
     * @return string|null
     */
    public static function extractFileStamp($alias, $fileName)
    {
        $originName = FileHelper::normalizePath(Yii::getAlias($alias, false));
        $origInfo = pathinfo($originName);
        $fileInfo = pathinfo($fileName);

        if ($origInfo['dirname'] === $fileInfo['dirname'] && $origInfo['extension'] === $fileInfo['extension'] && strpos($fileInfo['filename'], $origInfo['filename']) === 0) {
            return substr($fileInfo['filename'], strlen($origInfo['filename']) + 1);
        } else {
            return null;
        }
    }

    /**
     * @param string $level
     * @return string
     */
    public static function getLevelClass($level)
    {
        $levelClasses = [
            'trace' => 'label-default',
            'info' => 'label-info',
            'warning' => 'label-warning',
            'error' => 'label-danger',
        ];

        return array_key_exists($level, $levelClasses) ? $levelClasses[$level] : 'label-default';
    }

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        parent::__construct($config);

        if (empty($config['fileName'])) {
            $this->fileName = static::extractFileName($this->alias);
        }
    }

    /**
     * @return integer|null
     */
    public function getSize()
    {
        return file_exists($this->fileName) ? filesize($this->fileName) : null;
    }

    /**
     * @return integer|null
     */
    public function getUpdatedAt()
    {
        return file_exists($this->fileName) ? filemtime($this->fileName) : null;
    }

    /**
     * @param boolean $force
     * @return array
     */
    public function getCounts($force = false)
    {
        if (!file_exists($this->fileName)) {
            return [];
        }

        $key = $this->fileName . '.counts';

        if (!$force && ($counts = Yii::$app->cache->get($key)) !== false) {
            return $counts;
        }

        $counts = [];

        if ($h = fopen($this->fileName, 'r')) {
            while (($line = fgets($h)) !== false) {
                if (preg_match('/^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', $line)) {
                    if (preg_match('/^[\d\-\: ]+\[.*\]\[.*\]\[.*\]\[(.*)\]/U', $line, $m)) {
                        $level = $m[1];

                        if (!isset($counts[$level])) {
                            $counts[$level] = 0;
                        }

                        $counts[$level]++;
                    }
                }
            }

            fclose($h);

            Yii::$app->cache->set($key, $counts, 0, new FileDependency([
                'fileName' => $this->fileName,
            ]));
        }

        return $counts;
    }

    /**
     * @param string $stamp
     * @return boolean
     */
    public function archive($stamp)
    {
        if (file_exists($this->fileName)) {
            rename($this->fileName, self::extractFileName($this->alias, $stamp));

            return true;
        } else {
            return false;
        }
    }
}
