<?php
namespace backend\modules\home;

/**
 * Class Module
 * @package backend\modules\home
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\home\controllers';
}
