<?php
namespace backend\modules\home\controllers;

use backend\components\Controller;

/**
 * Class IndexController
 * @package backend\modules\home\controllers
 */
class IndexController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
