<?php
namespace backend\modules\order\controllers;

use backend\components\Controller;
use common\models\Item;
use common\models\Order;
use common\models\OrderItem;
use backend\models\search\OrderItemSearch;
use backend\models\search\OrderSearch;
use Yii;
use yii\helpers\Url;
use yii\web\HttpException;

/**
 * Class IndexController
 * @package backend\modules\order\controllers
 */
class IndexController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $modelSearch = new OrderSearch();
        $dataProvider = $modelSearch->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws HttpException
     */
    public function actionEdit($id = null)
    {
        $post = Yii::$app->request->post();
        $model = Order::find()
            ->joinWith('orderItems')
            ->where([Order::tableName() . '.id' => $id])
            ->one();

        if (!$model) {
            $model = new Order;
        } else {
            $model->totalCount = $model->getTotalPriceWithDiscount();
        }

        $orderItems = new OrderItemSearch();
        $itemsDataProvider = $orderItems->search([$orderItems->formName() => [
            'order_id' => $model->id
        ]]);

        $position = new OrderItem();

        if ($model->load($post) || $position->load($post)) {

            if ($model->save()) {
                Yii::$app->notifier->addNotifier(Yii::t('common', 'Заказ успешно отредактирован.'), 'success');
            } else {
                Yii::$app->notifier->addNotifierErrorByModel($model);
            }

            if (!$model->isNewRecord) {
                if ($position->load($post)) {
                    $itemPos = Item::findOne($position->item_id);

                    if (!$itemPos) {
                        throw new HttpException(404, Yii::t('common', 'Такой позиции не найдено'));
                    }

                    $position->price = $itemPos->price;

                    if ($position->save()) {
                        Yii::$app->notifier->addNotifier(Yii::t('common', 'Позиция успешно добавлена.'), 'success');
                    } else {
                        Yii::$app->notifier->addNotifierErrorByModel($position);
                    }
                }
            }

//            return $this->redirect(Url::toRoute(['edit', 'id' => $model->id]));
        }

        return $this->render('edit', [
            'model' => $model,
            'position' => $position,
            'itemsDataProvider' => $itemsDataProvider
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionDeletePosition($id)
    {
        /** @var OrderItem $model */
        $model = OrderItem::findOne($id);

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Невозможно найти указанную позицию'));
        }

        if ($model->delete()) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Позиция успешно удалена.'), 'success');
        } else {
            Yii::$app->notifier->addNotifierErrorByModel($model);
        }

        return $this->redirect(Url::toRoute(['edit', 'id' => $model->order_id]));
    }

    /**
     * @param $id
     * @return \yii\web\Response
     */
    public function actionSendScore($id)
    {
        $order = $this->getModel($id);

        if (!$order->customer_email) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Email не может быть пустым.'), 'error');

            return $this->redirect(['/order/index/edit', 'id' => $order->id]);
        }

        $order->totalCount = $order->getTotalPriceWithDiscount();

        if ($this->sendMessageScore($order)) {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'Счёт успешно отправлен.'), 'success');

            $order->status = Order::STATUS_SCORE_SENT;
            $order->save();
        } else {
            Yii::$app->notifier->addNotifier(Yii::t('common', 'При отправке письма возникла ошибка, возможно некорректный email.'), 'error');
        }

        return $this->redirect(['/order/index/edit', 'id' => $order->id]);
    }

    /**
     * @param integer $id
     * @return Order
     * @throws HttpException
     */
    private function getModel($id)
    {
        $model = Order::find()
            ->joinWith('orderItems')
            ->where([Order::tableName() . '.id' => $id])
            ->one();

        if (!$model) {
            throw new HttpException(404, Yii::t('common', 'Заказ не найден.'));
        }

        return $model;
    }

    /**
     * @param Order $order
     * @return bool
     */
    private function sendMessageScore($order)
    {
        try {
            Yii::$app->mailer
                ->compose('orderScore', [
                    'order' => $order
                ])
                ->setFrom(Yii::$app->params['noReplyEmail'])
                ->setTo($order->customer_email)
                ->setSubject(Yii::t('common', 'Счёт на оплату заказа ' . Yii::$app->params['urlFrontend']))
                ->send();

            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}
