<?php
use backend\components\grid\ActionColumn;
use backend\helpers\grid\DataProvider;
use backend\models\search\OrderSearch;
use backend\widgets\Panel;
use common\models\Order;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;

/** @var yii\web\View $this */
/** @var OrderSearch $modelSearch */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('common', 'Список заказов');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Список заказов'),
    'content' => GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            [
                'label' => Yii::t('common', 'ФИО'),
                'content' => function (Order $model) {
                    return $model->surname . ' ' . $model->name . ' ' . $model->patronymic;
                }
            ],
            'phone',
            'email',
            [
                'attribute' => 'status',
                'content' => function ($model) {
                    return Order::getStatuses()[$model->status];
                }
            ],
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d-m-Y H:i']
            ],
            [
                'class' => ActionColumn::className(),
                'items' => [
                    [
                        'label' => Yii::t('common', 'Детальный просмотр'),
                        'url' => function ($model) {
                            return Url::toRoute(['edit', 'id' => $model->id]);
                        }
                    ],
                ]
            ]
        ],
    ]),
    'footerContent' => LinkPager::widget([
            'pagination' => $dataProvider->getPagination()
        ]) .
        Html::a(Yii::t('common', 'Добавить заказ'), ['/order/index/edit'], ['class' => 'btn btn-sm btn-success']),
]) ?>
