<?php
use backend\components\grid\ActionColumn;
use backend\models\search\OrderItemSearch;
use backend\widgets\Panel;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var \yii\web\View $this */
/** @var \common\models\Order $model */
/** @var \common\models\OrderItem $position */
/** @var OrderItemSearch $itemsDataProvider */

$this->title = Yii::t('common', 'Редактирование заказа');
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'Заказы'), 'url' => '#'];
$this->params['breadcrumbs'][] = ['label' => $this->title];
?>

<?php Pjax::begin() ?>

<?= Panel::widget([
    'title' => Yii::t('common', 'Заказ'),
    'content' => $this->render('_edit-form', [
        'model' => $model,
    ])
]); ?>

<?php if (!$model->isNewRecord): ?>
    <?= Panel::widget([
        'title' => Yii::t('common', 'Добавление новой позиции'),
        'content' => $this->render('_position-order', [
            'position' => $position,
            'order' => $model,
        ])
    ]); ?>

    <?= Panel::widget([
        'title' => Yii::t('common', 'Заказанные товары'),
        'content' => GridView::widget([
            'dataProvider' => $itemsDataProvider,
            'columns' => [
                'id',
                [
                    'attribute' => 'item.name',
                    'content' => function ($model) {
                        return Html::a($model->item->name, Url::to('/catalog/item/edit/' . $model->item_id), ['target' => '_blank']);
                    }
                ],
                [
                    'attribute' => 'quantity',
                    'enableSorting' => false
                ],
                'item.cost',
                [
                    'class' => ActionColumn::className(),
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Удалить позицию'),
                            'url' => function () {
                                return '#';
                            },
                            'style' => 'confirm-delete-link',
                            'attributes' => function ($model) {
                                return [
                                    'data-href' => Url::toRoute(['delete-position', 'id' => $model->id]),
                                ];
                            },
                        ],
                    ]
                ]
            ],
        ]),
        'footerContent' => $this->render('_footer-total', [
            'order' => $model
        ])
    ]);
    ?>
<?php endif; ?>
<?php Pjax::end(); ?>

<? //= ModalConfirmDelete::widget(); ?>

