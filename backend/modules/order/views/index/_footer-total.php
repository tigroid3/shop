<?php
use common\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var Order $order */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $order->id])
]); ?>
<div class="row">
    <div class="col-lg-8"></div>
    <div class="col-lg-2">
        <?= $form->field($order, 'discount')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($order, 'totalCount')->textInput(['disabled' => 'disabled']) ?>
    </div>
</div>
<div class="row text-right">
    <div class="col-lg-12">
        <?= Html::submitInput(Yii::t('common', 'Посчитать скидку'), [
            'class' => 'btn btn-success'
        ]) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
