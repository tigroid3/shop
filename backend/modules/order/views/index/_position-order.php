<?php
use common\models\Item;
use common\models\Order;
use common\models\OrderItem;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var OrderItem $position */
/** @var Order $order */

?>
<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $order->id])
]); ?>

<div class="row">
    <?= Html::activeHiddenInput($position, 'order_id', ['value' => $order->id]) ?>
    <div class="col-xs-3">
        <?= $form->field($position, 'item_id')->widget(Select2::className(), [
            'data' => Item::find()->collection(),
            'pluginOptions' => [
                'prompt' => Yii::t('common', 'Выберите товар'),
            ],
            'id' => 'pos_add_item',
        ]) ?>
    </div>
    <div class="col-xs-3">
        <?= $form->field($position, 'quantity')->textInput([
            'value' => 1
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= Html::submitInput(Yii::t('common', 'Добавить позицию'), [
            'class' => 'btn btn-primary btn-sm'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), ['index'], [
            'class' => 'btn btn-default btn-sm'
        ]); ?>
    </div>
</div>

<?php ActiveForm::end() ?>


