<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/** @var \common\models\Order $model */
?>

<?php $form = ActiveForm::begin([
    'action' => Url::toRoute(['edit', 'id' => $model->id])
]); ?>
<div class="row">
    <div class="col-lg-2">
        <?= $form->field($model, 'created_at')->textInput([
            'value' => date('d-m-Y H:i', $model->created_at),
            'readonly' => true
        ]) ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?=$form->field($model, 'name') ?>
    </div>
    <div class="col-sm-4">
        <?=$form->field($model, 'patronymic') ?>
    </div>
    <div class="col-sm-4">
        <?=$form->field($model, 'surname') ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-3">
        <?=$form->field($model, 'phone') ?>
    </div>
    <div class="col-sm-3">
        <?=$form->field($model, 'email') ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <?=$form->field($model, 'address') ?>
    </div>
    <div class="col-sm-2">
        <?=$form->field($model, 'home_number') ?>
    </div>
    <div class="col-sm-2">
        <?=$form->field($model, 'apartment_number') ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= $form->field($model, 'comment')->textarea() ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?= Html::submitInput(Yii::t('common', 'Сохранить заказ'), [
            'class' => 'btn btn-success'
        ]) ?>
        <?= Html::a(Yii::t('common', 'Отмена'), ['index']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
