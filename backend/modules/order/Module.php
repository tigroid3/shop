<?php
namespace backend\modules\order;

/**
 * Class Module
 * @package backend\modules\order
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\order\controllers';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [];
    }

}
