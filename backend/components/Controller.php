<?php
namespace backend\components;

use Yii;

/**
 * Class Controller
 * @package backend\components
 */
class Controller extends \yii\web\Controller
{
    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            $this->redirect('/auth/login');

            return false;
        }

        return parent::beforeAction($action);
    }
}
