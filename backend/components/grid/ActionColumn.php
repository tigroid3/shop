<?php
namespace backend\components\grid;

use backend\widgets\DropdownActions;
use yii\base\Model;
use yii\grid\Column;

/**
 * Class ActionColumn
 * @package backend\components\grid
 */
class ActionColumn extends Column
{
    /** @var array */
    public $headerOptions = ['class' => 'th-actions'];

    /** @var array */
    public $contentOptions = ['class' => 'td-actions'];

    /** @var boolean */
    public $divider = false;

    /** @var array */
    public $items = [];

    /**
     * @param Model $model
     * @param integer $key
     * @param integer $index
     * @return string
     */
    protected function renderDataCellContent($model, $key, $index)
    {
        $items = [];

        foreach ($this->items as $key => $item) {

            if (isset($item['can'])) {
                if ($item['can']($model)) {
                    $items[$key] = [
                        'label' => $item['label'],
                        'url' => $item['url']($model),
                    ];
                }
            } else {
                $items[$key] = [
                    'label' => $item['label'],
                    'url' => $item['url']($model),
                ];
            }
        }

        return DropdownActions::widget([
            'links' => $items
        ]);
    }
}
