$(function () {
    $(document).on('click', '.confirm-delete-link', ModalConfirmDelete.confirmDelete);
    $(document).on('click', '.confirm-action-link', ModalConfirmDelete.confirmAction);
});

var ModalConfirmDelete = {
    confirmDelete: function () {
        $('#modal_confirm_delete_link').attr('href', $(this).data('href'));
        $('#modal_confirm_delete').modal();

        return false;
    },
    confirmAction: function () {
        $('#modal_confirm_action_link').attr('href', $(this).data('href'));
        $('#modal_confirm_action').modal();

        return false;
    },
};
