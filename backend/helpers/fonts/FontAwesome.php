<?php
namespace backend\helpers\fonts;

use yii\base\Object;

/**
 * Class FontAwesome
 * @package backend\helpers\fonts
 */
class FontAwesome extends Object
{
    const DATABASE = 'fa fa-database';
    const COG = 'fa fa-cog';
    const HOME = 'fa fa-home';
    const EXCLAMATION = 'fa fa-exclamation-circle';
    const PENCIL = 'fa fa-pencil-square-o';
    const IN_BOX = 'fa fa-inbox ';
    const EYE = 'fa fa-eye ';
}
