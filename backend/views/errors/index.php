<?php
use yii\helpers\Html;

/** @var \yii\web\View $this */
/** @var \yii\web\HttpException $exception */
/** @var string $name */
/** @var string $message */

$this->title = $name;
?>

<div class="container body">
    <div class="main_container">
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center">
                    <h1 class="error-number"><?= $exception->statusCode ?></h1>
                    <h2><?= Yii::t('common', 'При обработке вашего запроса произошла ошибка.') ?></h2>
                    <p> <?= nl2br(Html::encode($message)) ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
