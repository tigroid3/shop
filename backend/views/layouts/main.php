<?php
use backend\assets\ThemeAsset;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\web\View;

/** @var string $content */
/** @var View $this */

ThemeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head class="nav-md">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="nav-md">
<?php $this->beginBody() ?>
<div class="container body">
    <div class="main_container">
        <?= $this->render('../common/_aside') ?>

        <?= $this->render('../common/_header') ?>

        <div class="right_col" role="main">
            <?= $content ?>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
<?= $this->render('../common/_notifiers') ?>
</body>
</html>
<?php $this->endPage() ?>
