<?php
use backend\helpers\fonts\FontAwesome;
use yii\helpers\Url;

?>

<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li role="presentation" class="dropdown">
                    <a href="<?= Url::to(['/administration/runtime/index']) ?>">
                        <i class="<?= FontAwesome::EXCLAMATION ?>"></i><span> <?= Yii::t('common', 'Логи') ?></span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
