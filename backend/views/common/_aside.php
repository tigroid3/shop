<?php
use backend\helpers\fonts\FontAwesome;
use backend\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

$moduleId = Yii::$app->controller->module->id;;
$controllerId = Yii::$app->controller->id;
?>

<div class="col-md-3 left_col">
    <div class="left_col scroll-view">
        <div class="navbar nav_title" style="border: 0;">
            <a href="/" class="site_title"><i class="fa fa-paw"></i> <span><?= Yii::t('common', 'Admin panel') ?></span></a>
        </div>
        <div class="profile clearfix">
            <div class="profile_pic">
                <?= Html::img('/resources/images/user.png', [
                    'class' => 'img-circle profile_img'
                ]) ?>
            </div>
            <div class="profile_info">
                <span><?= Yii::t('common', 'Добро пожаловать') ?>,</span>
                <h2><?= Yii::$app->user->identity->username ?></h2>
            </div>
        </div>
        <br/>
        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
                <?= Menu::widget([
                    'options' => [
                        'class' => 'nav side-menu',
                    ],
                    'items' => [
                        [
                            'label' => Yii::t('common', 'Главная страница'),
                            'icon' => FontAwesome::HOME,
                            'url' => Url::home()
                        ],
                        [
                            'label' => Yii::t('common', 'Каталог'),
                            'icon' => FontAwesome::DATABASE,
                            'url' => '#',
                            'active' => $moduleId == 'catalog',
                            'items' => [
                                [
                                    'label' => Yii::t('common', 'Категории'),
                                    'url' => ['/catalog/category'],
                                    'active' => $moduleId == 'catalog' && $controllerId == 'category',
                                ],
                                [
                                    'label' => Yii::t('common', 'Товары'),
                                    'url' => ['/catalog/item'],
                                    'active' => $moduleId == 'catalog' && $controllerId == 'item',
                                ],
                                [
                                    'label' => Yii::t('common', 'Бренды'),
                                    'url' => ['/catalog/brand'],
                                    'active' => $moduleId == 'catalog' && $controllerId == 'brand',
                                ],
                            ],
                        ],
                        [
                            'label' => Yii::t('common', 'Заказы'),
                            'icon' => FontAwesome::IN_BOX,
                            'active' => $moduleId == 'order',
                            'url' => ['/order/index']
                        ],
                        [
                            'label' => Yii::t('common', 'Контент'),
                            'icon' => FontAwesome::PENCIL,
                            'active' => $moduleId == 'content',
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => Yii::t('common', 'Страницы'),
                                    'url' => ['/content/page'],
                                    'active' => $moduleId == 'content' && $controllerId == 'page',
                                ],
                                [
                                    'label' => Yii::t('common', 'Новости'),
                                    'url' => ['/content/news'],
                                    'active' => $moduleId == 'content' && $controllerId == 'news',
                                ],
                                [
                                    'label' => Yii::t('common', 'Статьи'),
                                    'url' => ['/content/article'],
                                    'active' => $moduleId == 'content' && $controllerId == 'article',
                                ],
                            ]
                        ],
                        [
                            'label' => Yii::t('common', 'Настройки'),
                            'icon' => FontAwesome::COG,
                            'active' => $moduleId == 'administration' && $controllerId == 'options',
                            'url' => ['/administration/options']
                        ],
                    ]
                ]) ?>
            </div>
        </div>
        <div class="sidebar-footer hidden-small">
            <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?= Url::to('/auth/logout') ?>"
               class="full-width">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
            </a>
        </div>
    </div>
</div>
