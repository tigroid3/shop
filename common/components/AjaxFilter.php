<?php
namespace common\components;

use Yii;
use yii\base\ActionFilter;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Class AjaxFilter
 * @package common\components
 */
class AjaxFilter extends ActionFilter
{
    /**
     * @param \yii\base\Action $action
     * @return boolean
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return parent::beforeAction($action);
        }

        throw new BadRequestHttpException(Yii::t('common', 'Разрешен только Ajax запрос.'));
    }
}
