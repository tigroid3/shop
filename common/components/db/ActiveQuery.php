<?php
namespace common\components\db;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class ActiveQuery
 * @package common\components\db
 */
class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * @var string
     */
    protected $collectionKey = 'id';

    /**
     * @var string
     */
    protected $collectionValue = 'name';

    /**
     * @param string|null $translateCategory
     * @return array
     */
    public function collection($translateCategory = null)
    {
        $items = ArrayHelper::map($this->all(), $this->collectionKey, $this->collectionValue);

        if (!is_null($translateCategory)) {
            foreach ($items as $key => $item) {
                $items[$key] = Yii::t($translateCategory, $item);
            }
        }

        return $items;
    }
}
