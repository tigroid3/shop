<?php
namespace common\components\db;

use yii\behaviors\TimestampBehavior;

/**
 * Class ActiveRecord
 * @package common\components\db
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function shortClassName()
    {
        $model = new static();
        $reflect = new \ReflectionClass($model);

        return $reflect->getShortName();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @param null $attribute
     * @return array
     */
    public function getErrorsAsArray($attribute = null)
    {
        $result = [];

        $errorsAttributes = $this->getErrors($attribute);

        foreach ($errorsAttributes as $errorsAttribute) {
            foreach ($errorsAttribute as $error) {
                $result[] = $error;
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getFirstErrorAsString()
    {
        $errors = $this->getErrorsAsArray();

        return array_shift($errors);
    }
}
