<?php
namespace common\components;

use common\models\Category;
use common\models\Item;
use common\models\Lead;
use common\models\Order;
use common\models\OrderItem;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class UploaderCsv
 * @package common\components
 */
class UploaderCsv extends Component
{
    /**
     * Выгрузка
     */
    public function upload()
    {
        $this->getCategories();
        $this->getItems();
    }

    /**
     * @return bool
     */
    public function getItems()
    {
        $path = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'unloading' . DIRECTORY_SEPARATOR . 'items.csv';

        if (!file_exists($path)) {
            return false;
        }

        Item::updateAll(['show' => Item::NO]);

        if (($handle = fopen($path, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                $item = Item::findOne(['code' => $data[1]]);

                if (!$item) {
                    $item = new Item();
                    $item->code = $data[1];
                }

                $item->category_id = $this->getCorrectId($data[0]);
                $item->name = $data[2];
                $item->unit = $data[3];
                $item->weight = $data[4];
                $item->height = $data[5];
                $item->show = Item::YES;
                $item->cost = floatval(str_replace(',', '.', $data[6]));
                $item->save();
            }

            fclose($handle);
        }
    }

    /**
     * @return bool
     */
    public function getCategories()
    {
        $path = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'unloading' . DIRECTORY_SEPARATOR . 'category.csv';

        if (!file_exists($path)) {
            return false;
        }

        if (($handle = fopen($path, "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ";")) !== false) {
                $category = Category::findOne($this->getCorrectId($data[1]));

                if (!$category) {
                    $category = new Category();
                }

                $category->id = $this->getCorrectId($data[1]);
                $category->parent_id = $this->getCorrectId($data[2]);
                $category->name = $data[0];
                $category->save();
            }

            fclose($handle);
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getCorrectId($id)
    {
        return (int)str_replace('00-0000', 1, $id);
    }
}
