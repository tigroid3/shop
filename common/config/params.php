<?php
return [
    'adminEmail' => 'sarge_tigra@mail.ru',
    'supportEmail' => 'sarge_tigra@mail.ru',
    'imgSizes' => [
        'itemSmallWidth' => 100,
        'itemSmallHeight' => 100,
        'itemMediumWidth' => 200,
        'itemMediumHeight' => 200,
        'itemBigWidth' => 350,
        'itemBigHeight' => 350,
        'categoryWidth' => 200,
        'categoryHeight' => 200,
        'brandWidth' => 200,
        'brandHeight' => 200,
        'newsWidth' => 150,
        'newsHeight' => 150,
    ]
];
