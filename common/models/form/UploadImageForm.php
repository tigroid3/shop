<?php
namespace common\models\form;

use common\helpers\Image;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadImageForm
 * @package common\models\form
 */
class UploadImageForm extends Model
{
    /** @var UploadedFile[] */
    public $files;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 10],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'files' => Yii::t('common', 'Фото')
        ];
    }

    /**
     * @return array|bool
     */
    public function upload()
    {
        if ($this->validate()) {
            $successFiles = [];

            foreach ($this->imageFiles as $file) {
                $fileName = Image::getUniqName() . '.' . $file->extension;

                if ($file->saveAs(Image::preparePathUpload($fileName, true) . $fileName)) {
                    $successFiles[] = $fileName;
                }
            }

            return $successFiles;
        } else {
            return false;
        }
    }

}
