<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\helpers\Image;
use common\helpers\MyHelper;
use common\models\query\BrandQuery;
use Yii;
use yii\web\UploadedFile;

/**
 * Class Brand
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $photo
 * @property string $created_at
 * @property string $updated_at
 */
class Brand extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%brand}}';
    }

    /**
     * @return BrandQuery
     */
    public static function find()
    {
        return new BrandQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['photo', 'alias'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'alias' => Yii::t('common', 'Алиас'),
            'photo' => Yii::t('common', 'Фото'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->alias) {
            $this->alias = MyHelper::translate($this->name);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param UploadedFile[] $imageFile
     * @return bool
     */
    public function uploadImage($imageFile)
    {
        if ($imageFile) {
            $fileName = Image::getUniqName() . '.' . $imageFile[0]->extension;
            $pathFile = Image::preparePathUpload($fileName, true, Image::SUB_DIR_BRANDS, true);

            $oldPhoto = $this->photo;

            if ($imageFile[0]->saveAs($pathFile)) {
                $this->photo = $fileName;

                Image::imageThumb($pathFile, $pathFile,
                    Yii::$app->params['imgSizes']['brandWidth'],
                    Yii::$app->params['imgSizes']['brandHeight']
                );

                @unlink(Image::preparePathUpload($oldPhoto, false, Image::SUB_DIR_BRANDS, true));
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->photo) {
            @unlink(Image::preparePathUpload($this->photo, false, Image::SUB_DIR_BRANDS, true));
        }

        return parent::beforeDelete();
    }
}
