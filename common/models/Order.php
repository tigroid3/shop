<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\OrderQuery;
use Yii;

/**
 * Class Order
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $home_number
 * @property string $apartment_number
 * @property string $number_track
 * @property string $patronymic
 * @property string $surname
 * @property string $customer_city
 * @property string $customer_address
 * @property string $comment
 * @property string $method
 * @property string $status
 * @property string $delivery
 * @property integer $discount
 * @property string $delivery_date
 * @property string $hash_session
 * @property string $created_at
 * @property string $updated_at
 *
 * @property OrderItem[] $orderItems
 */
class Order extends ActiveRecord
{
    public $totalCount = 0;

    const STATUS_NEW = 1; //новый
    const STATUS_IN_PROGRESS = 2; //в процессе
    const STATUS_PAID = 3; //оплачен
    const STATUS_WAIT_PAID = 3; //ожидает оплаты
    const STATUS_DONE = 4; //завершен

    const PAY_METHOD_COD = 1; //оплата наличными при получении
    const PAY_METHOD_ELECTRO = 2; //оплата банковской картой

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => Yii::t('common', 'Новый'),
            self::STATUS_IN_PROGRESS => Yii::t('common', 'В обработке'),
            self::STATUS_PAID => Yii::t('common', 'Оплачено'),
            self::STATUS_DONE => Yii::t('common', 'Завершен'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @return OrderQuery
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [
                [
                    'status',
                    'method',
                ], 'integer'
            ],
            ['discount', 'double', 'min' => 0],
            [
                [
                    'name',
                    'surname',
                    'patronymic',
                    'phone',
                    'discount',
                    'address',
                    'home_number',
                    'apartment_number',
                    'comment',
                    'hash_session',
                ], 'string'
            ],
            ['email', 'email'],
            ['status', 'default', 'value' => self::STATUS_NEW],
            ['status', 'in', 'range' => array_keys(self::getStatuses())],
            ['totalCount', 'safe'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Имя'),
            'patronymic' => Yii::t('common', 'Отчество'),
            'surname' => Yii::t('common', 'Фамилия'),
            'phone' => Yii::t('common', 'Телефон'),
            'address' => Yii::t('common', 'Адрес'),
            'home_number' => Yii::t('common', 'Номер дома'),
            'apartment_number' => Yii::t('common', 'Номер квартиры'),
            'comment' => Yii::t('common', 'Комментарий'),
            'status' => Yii::t('common', 'Статус'),
            'email' => Yii::t('common', 'Email'),
            'discount' => Yii::t('common', 'Скидка %'),
            'delivery_date' => Yii::t('common', 'Дата доставки'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return int
     */
    public function getTotalPriceByModel()
    {
        $totalSum = 0;

        /** @var Item $item */
        foreach ($this->orderItems as $item) {
            $totalSum += $item->price * $item->quantity;
        }

        return $totalSum;
    }

    /**
     * @return int
     */
    public function getTotalPriceWithDiscount()
    {
        $totalPrice = $this->getTotalPriceByModel();

        return round($totalPrice - ($totalPrice * ($this->discount / 100)));
    }
}
