<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\helpers\Image;
use common\helpers\MyHelper;
use common\models\query\CategoryQuery;
use Yii;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * Class Category
 * @package common\models
 * @property integer $id
 * @property integer $parent_id
 * @property integer $code
 * @property string $name
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $description
 * @property string $photo
 * @property string $video
 * @property string $class
 * @property integer $position
 * @property integer $alias
 * @property integer $active
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Item[] $items
 * @property Category $parent
 */
class Category extends ActiveRecord
{
    const ROOT_CATEGORY = 1;

    /**
     * @return array
     */
    public static function getShowCollection()
    {
        return [
            NO => Yii::t('common', 'Не показывать'),
            YES => Yii::t('common', 'Показывать')
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'name'], 'required'],
            ['code', 'unique'],
            [['name', 'meta_title', 'meta_description', 'meta_keywords', 'description', 'photo', 'class', 'alias'], 'string'],
            [['position', 'code', 'parent_id', 'sort'], 'integer'],
            ['position', 'default', 'value' => 0],
            ['parent_id', 'compare', 'compareValue' => 0, 'operator' => '!='],
            ['active', 'in', 'range' => array_keys(self::getShowCollection())],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(Item::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', '№'),
            'parent_id' => Yii::t('common', 'Родительская категория'),
            'code' => Yii::t('common', 'Код'),
            'name' => Yii::t('common', 'Название'),
            'meta_title' => Yii::t('common', 'Мета заголовок'),
            'meta_description' => Yii::t('common', 'Мета описание'),
            'meta_keywords' => Yii::t('common', 'Ключевые слова'),
            'description' => Yii::t('common', 'Описание'),
            'photo' => Yii::t('common', 'Фото'),
            'position' => Yii::t('common', 'Позиция'),
            'active' => Yii::t('common', 'Активность'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'alias' => Yii::t('common', 'Алиас'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->alias) {
            $this->alias = MyHelper::translate($this->name);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return mixed
     */
    public function getUrlYoutube()
    {
        if ($this->video) {
            preg_match('/watch\?v=?(.*)/', $this->video, $answer);

            return 'https://www.youtube.com/embed/' . end($answer);
        }
    }

    /**
     * @param $ignoredCategory
     * @return array
     */
    public function getParentsCategories($ignoredCategory)
    {
        $categories = Category::find()
            ->where(['!=', 'id', $ignoredCategory ? $ignoredCategory : ''])
            ->all();

        return ArrayHelper::map($categories, 'id', 'name');
    }

    /**
     * @return array
     */
    public function getPathCategory()
    {
        $categories = Yii::$app->db->cache(function ($db) {
            $query = Category::find()
                ->asArray();

            /** @var Connection $db */
            return $db->createCommand($query->createCommand()->rawSql)->queryAll();
        }, 300);

        $requestCategories = [];

        $categoryId = $this->id;

        while (true) {

            $found = Category::searchCategory($categories, $categoryId);

            if ($found) {
                $categoryId = $found['parent_id'];
                $requestCategories[] = $found['alias'];
            } else {
                break;
            }
        }

        $requestCategories = array_reverse($requestCategories);
        unset($requestCategories[0]);

        return implode('/', $requestCategories);
    }


    /**
     * @param $arrayTree
     * @param $categoryId
     * @return mixed
     */
    public static function searchCategory($arrayTree, $categoryId)
    {
        $arr = array_filter($arrayTree, function ($a) use ($categoryId) {
            return $a['id'] == $categoryId;
        });

        if ($arr) {
            return current($arr);
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public static function getListCategories()
    {
        $categories = self::find()->collection();
        unset($categories[self::ROOT_CATEGORY]);

        return $categories;
    }

    /**
     * @param UploadedFile[] $imageFile
     * @return bool
     */
    public function uploadImage($imageFile)
    {
        if ($imageFile) {
            $fileName = Image::getUniqName() . '.' . $imageFile[0]->extension;
            $pathFile = Image::preparePathUpload($fileName, true, Image::SUB_DIR_CATEGORIES, true);

            $oldPhoto = $this->photo;

            if ($imageFile[0]->saveAs($pathFile)) {
                $this->photo = $fileName;

                Image::imageThumb($pathFile, $pathFile,
                    Yii::$app->params['imgSizes']['categoryWidth'],
                    Yii::$app->params['imgSizes']['categoryHeight']
                );

                @unlink(Image::preparePathUpload($oldPhoto, false, Image::SUB_DIR_CATEGORIES, true));
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->photo) {
            @unlink(Image::preparePathUpload($this->photo, false, Image::SUB_DIR_CATEGORIES, true));
        }

        return parent::beforeDelete();
    }
}
