<?php
namespace common\models;

use common\components\db\ActiveRecord;
use Yii;

/**
 * Class OrderItem
 * @package common\models
 * @property integer $id
 * @property integer $order_id
 * @property integer $item_id
 * @property integer $quantity
 * @property integer $price
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Item $item
 */
class OrderItem extends ActiveRecord
{
    public $article;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_item}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'item_id', 'price'], 'required'],
            ['price', 'double', 'min' => 0],
            [['order_id', 'item_id'], 'integer'],
            ['quantity', 'integer', 'min' => 1],
            ['quantity', 'default', 'value' => 1]
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(Item::className(), ['id' => 'item_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'order_id' => Yii::t('common', 'Заказ'),
            'item_id' => Yii::t('common', 'Товар'),
            'quantity' => Yii::t('common', 'Количество'),
            'price' => Yii::t('common', 'Цена'),
            'article' => Yii::t('common', 'Артикул'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }
}
