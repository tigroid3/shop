<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\helpers\Image as ImageHelper;
use common\models\query\ItemImageQuery;
use Imagine\Image\Box;
use Yii;
use yii\imagine\Image;

/**
 * Class ItemImage
 * @package common\models
 * @property integer $id
 * @property integer $item_id
 * @property integer $default
 * @property string $size_small
 * @property string $size_medium
 * @property string $size_big
 * @property string $original
 * @property integer $created_at
 * @property integer $updated_at
 */
class ItemImage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item_image}}';
    }

    /**
     * @return ItemImageQuery
     */
    public static function find()
    {
        return new ItemImageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id'], 'required'],
            ['default', 'integer'],
            [['size_small', 'size_medium', 'size_big', 'original'], 'string'],
            ['item_id', 'exist', 'targetClass' => '\common\models\Item', 'targetAttribute' => 'id', 'message' => Yii::t('common', 'Неверный id товара.')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'item_id' => Yii::t('common', 'Товар'),
            'size_small' => Yii::t('common', 'Маленькое фото'),
            'size_medium' => Yii::t('common', 'Среднее фото'),
            'size_big' => Yii::t('common', 'Большое фото'),
            'original' => Yii::t('common', 'Оригинальное фото'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        @unlink(ImageHelper::preparePathUpload($this->size_small, false, ImageHelper::SUB_DIR_ITEMS, true));
        @unlink(ImageHelper::preparePathUpload($this->size_medium, false, ImageHelper::SUB_DIR_ITEMS, true));
        @unlink(ImageHelper::preparePathUpload($this->size_big, false, ImageHelper::SUB_DIR_ITEMS, true));
        @unlink(ImageHelper::preparePathUpload($this->original, false, ImageHelper::SUB_DIR_ITEMS_ORIGINAL, true));

        return parent::beforeDelete();
    }
}
