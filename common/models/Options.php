<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\OptionsQuery;
use Yii;

/**
 * Class Page
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property string $alias
 * @property string $value
 * @property string $emails
 */
class Options extends ActiveRecord
{
    public $emails;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%options}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['value', 'name', 'alias'], 'string'],
            ['emails', 'safe']
        ];
    }

    /**
     * @return OptionsQuery
     */
    public static function find()
    {
        return new OptionsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Название'),
            'alias' => Yii::t('common', 'Значение'),
            'value' => Yii::t('common', 'Данные'),
            'emails' => Yii::t('common', 'Почта рассылки'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @return array
     */
    public static function getEmails()
    {
        $query = Options::find()->byAlias('emails')->one();

        if ($query) {
            $unserialaze = unserialize($query->value);
            $valid = [];

            foreach ($unserialaze as $key => $item) {
                if ($item != "") {
                    $valid[] = trim($item);
                }
            }

            return $valid;
        }

        return [];
    }
}
