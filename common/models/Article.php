<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\models\query\ArticleQuery;
use Yii;

/**
 * Class Article
 * @package common\models
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $short_content
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 */
class Article extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @return ArticleQuery
     */
    public static function find()
    {
        return new ArticleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['meta_title', 'meta_description', 'meta_keywords', 'short_content'], 'string'],
            [['content'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'title' => Yii::t('common', 'Заголовок'),
            'meta_title' => Yii::t('common', 'Мета заголовок'),
            'meta_description' => Yii::t('common', 'Мета описание'),
            'meta_keywords' => Yii::t('common', 'Мета ключевые слова'),
            'short_content' => Yii::t('common', 'Краткое описание'),
            'content' => Yii::t('common', 'Контент'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }
}
