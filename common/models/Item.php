<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\helpers\Image;
use common\helpers\MyHelper;
use common\models\query\ItemQuery;
use Yii;
use yii\web\UploadedFile;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * Class Item
 * @package common\models
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property integer $brand_id
 * @property string $description
 * @property string $video
 * @property string $code
 * @property double $price
 * @property string $unit
 * @property integer $hit
 * @property integer $alias
 * @property integer $discount
 * @property integer $reserve
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ItemImage[] $images
 * @property ItemImage $defaultImage
 * @property Brand $brand
 * @property Category $category
 */
class Item extends ActiveRecord implements CartPositionInterface
{
    use CartPositionTrait;

    /** @var UploadedFile[] */
    public $imageFiles;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%item}}';
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ItemQuery
     */
    public static function find()
    {
        return new ItemQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'required'],
            [['category_id', 'brand_id', 'discount', 'reserve'], 'integer'],
            ['price', 'double', 'min' => 0],
            [['name', 'code', 'video', 'alias'], 'string'],
            [['reserve'], 'default', 'value' => 0],
            [['name'], 'filter', 'filter' => 'trim'],
            ['name', 'string', 'max' => 256, 'tooLong' => Yii::t('common', 'Слишком длинное название.')],
            [['description', 'hit'], 'safe'],
            [['imageFiles'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 10],
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$this->alias) {
            $this->alias = MyHelper::translate($this->name) . '-' . $this->id;
            $this->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'category_id' => Yii::t('common', 'Категория'),
            'brand_id' => Yii::t('common', 'Бренд'),
            'name' => Yii::t('common', 'Название'),
            'description' => Yii::t('common', 'Описание'),
            'video' => Yii::t('common', 'URL видео'),
            'code' => Yii::t('common', 'Артикул'),
            'price' => Yii::t('common', 'Цена'),
            'hit' => Yii::t('common', 'Популярное'),
            'discount' => Yii::t('common', 'Скидка'),
            'reserve' => Yii::t('common', 'В наличии шт.'),
            'category.name' => Yii::t('common', 'Название категории'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'imageFiles' => Yii::t('common', 'Фото'),
        ];
    }

    /**
     * @return string
     */
    public function getFullUrl()
    {
        $pathCategory = $this->category->getPathCategory();

        return $pathCategory . '/' . $this->alias;
    }

    /**
     * @return mixed
     */
    public function getUrlYoutube()
    {
        if ($this->video) {
            preg_match('/watch\?v=?(.*)/', $this->video, $answer);

            return 'https://www.youtube.com/embed/' . end($answer);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(ItemImage::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDefaultImage()
    {
        return $this->hasOne(ItemImage::className(), ['item_id' => 'id'])
            ->where([ItemImage::tableName() . '.default' => 1,])
            ->orWhere(['IS', ItemImage::tableName() . '.id', null]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $images = ItemImage::find()
            ->where(['item_id' => $this->id])
            ->all();

        foreach ($images as $image) {
            $image->delete();
        }

        return parent::beforeDelete();
    }

    /**
     *
     */
    public function uploadImages()
    {
        if ($this->imageFiles && $this->validate()) {
            foreach ($this->imageFiles as $file) {
                $fileName = Image::getUniqName() . '.' . $file->extension;

                if ($file->saveAs(Image::preparePathUpload($fileName, true, Image::SUB_DIR_ITEMS_ORIGINAL, true))) {
                    $itemImage = new ItemImage();
                    $itemImage->item_id = $this->id;
                    $itemImage->original = $fileName;

                    if ($itemImage->save()) {
                        self::resizeImage($itemImage);
                    }
                }
            }
        }
    }

    /**
     * @param ItemImage $adImage
     */
    public static function resizeImage($adImage)
    {
        $sizes = self::getSizesAndFields();

        foreach ($sizes as $size) {
            $photoWithPfx = Image::getNameWithPrefix($adImage->original, $size['pfx']);

            Image::imageThumb(
                Image::preparePathUpload($adImage->original, false, Image::SUB_DIR_ITEMS_ORIGINAL, true),
                Image::preparePathUpload($photoWithPfx, true, Image::SUB_DIR_ITEMS, true),
                $size['width'],
                $size['height']
            );

            $adImage->{$size['field']} = $photoWithPfx;
        }

        if (!$adImage->save()) {
            print_r($adImage->getErrors());
        }
    }

    /**
     * Сюда добавляются нужные размеры которые нужно кропать
     * @return array
     */
    public static function getSizesAndFields()
    {
        return [
            [
                'width' => Yii::$app->params['imgSizes']['itemSmallWidth'],
                'height' => Yii::$app->params['imgSizes']['itemSmallHeight'],
                'field' => 'size_small',
                'pfx' => Image::PFX_SMALL,
            ],
            [
                'width' => Yii::$app->params['imgSizes']['itemMediumWidth'],
                'height' => Yii::$app->params['imgSizes']['itemMediumHeight'],
                'field' => 'size_medium',
                'pfx' => Image::PFX_MEDIUM,
            ],
            [
                'width' => Yii::$app->params['imgSizes']['itemBigWidth'],
                'height' => Yii::$app->params['imgSizes']['itemBigHeight'],
                'field' => 'size_big',
                'pfx' => Image::PFX_BIG,
            ],
        ];
    }
}
