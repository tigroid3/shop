<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\helpers\MyHelper;
use common\models\query\PageQuery;
use Yii;

/**
 * Class Page
 * @package common\models
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $content
 * @property string $alias
 * @property integer $visible
 * @property string $created_at
 * @property string $updated_at
 */
class Page extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @return PageQuery
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alias', 'title'], 'required'],
            ['visible', 'integer'],
            [['alias', 'meta_title', 'meta_description', 'meta_keywords', 'alias'], 'string'],
            ['alias', 'string', 'min' => 5],
            [['content'], 'safe'],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->alias) {
            $this->alias = MyHelper::translate($this->title);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'name' => Yii::t('common', 'Имя страницы'),
            'title' => Yii::t('common', 'Заголовок'),
            'meta_title' => Yii::t('common', 'Мета заголовок'),
            'meta_description' => Yii::t('common', 'Мета описание'),
            'meta_keywords' => Yii::t('common', 'Мета ключевые слова'),
            'content' => Yii::t('common', 'Контент'),
            'visible' => Yii::t('common', 'Показывать'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
            'alias' => Yii::t('common', 'Алиас'),
        ];
    }
}
