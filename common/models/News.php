<?php
namespace common\models;

use common\components\db\ActiveRecord;
use common\helpers\Image;
use common\helpers\MyHelper;
use common\models\query\NewsQuery;
use Yii;
use yii\web\UploadedFile;

/**
 * Class News
 * @package common\models
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $content
 * @property string $short_content
 * @property string $photo
 * @property string $alias
 * @property string $created_at
 * @property string $updated_at
 */
class News extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @return NewsQuery
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['meta_title', 'meta_description', 'meta_keywords', 'photo', 'alias'], 'string'],
            [['content', 'short_content'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'Номер'),
            'title' => Yii::t('common', 'Название'),
            'alias' => Yii::t('common', 'Алиас'),
            'meta_title' => Yii::t('common', 'Мета заголовок'),
            'meta_description' => Yii::t('common', 'Мета описание'),
            'meta_keywords' => Yii::t('common', 'Мета ключевые слова'),
            'photo' => Yii::t('common', 'Фото'),
            'content' => Yii::t('common', 'Контент'),
            'short_content' => Yii::t('common', 'Краткое описание'),
            'created_at' => Yii::t('common', 'Дата создания'),
            'updated_at' => Yii::t('common', 'Дата обновления'),
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->alias) {
            $this->alias = MyHelper::translate($this->title);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param UploadedFile[] $imageFile
     * @return bool
     */
    public function uploadImage($imageFile)
    {
        if ($imageFile) {
            $fileName = Image::getUniqName() . '.' . $imageFile[0]->extension;
            $pathFile = Image::preparePathUpload($fileName, true, Image::SUB_DIR_NEWS, true);

            $oldPhoto = $this->photo;

            if ($imageFile[0]->saveAs($pathFile)) {
                $this->photo = $fileName;

                Image::imageThumb($pathFile, $pathFile,
                    Yii::$app->params['imgSizes']['newsWidth'],
                    Yii::$app->params['imgSizes']['newsHeight']
                );

                @unlink(Image::preparePathUpload($oldPhoto, false, Image::SUB_DIR_NEWS, true));
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if ($this->photo) {
            @unlink(Image::preparePathUpload($this->photo, false, Image::SUB_DIR_NEWS, true));
        }

        return parent::beforeDelete();
    }
}
