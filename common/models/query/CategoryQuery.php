<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Category;

/**
 * Class CategoryQuery
 * @package common\models\query
 * @method Category one($db = null)
 * @method Category[] all($db = null)
 */
class CategoryQuery extends ActiveQuery
{
    
}
