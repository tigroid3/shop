<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Options;

/**
 * Class OptionsQuery
 * @package common\models\query
 * @method Options one($db = null)
 * @method Options[] all($db = null)
 */
class OptionsQuery extends ActiveQuery
{
    /**
     * @param $alias
     * @return $this
     */
    public function byAlias($alias)
    {
        return $this->andWhere([Options::tableName() . '.alias' => $alias]);
    }
}
