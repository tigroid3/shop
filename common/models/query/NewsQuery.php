<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\News;

/**
 * Class BrandQuery
 * @package common\models\query
 * @method News one($db = null)
 * @method News[] all($db = null)
 */
class NewsQuery extends ActiveQuery
{

}
