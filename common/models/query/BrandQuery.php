<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Brand;

/**
 * Class BrandQuery
 * @package common\models\query
 * @method Brand one($db = null)
 * @method Brand[] all($db = null)
 */
class BrandQuery extends ActiveQuery
{

}
