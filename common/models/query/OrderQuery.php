<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Order;

/**
 * Class OrderQuery
 * @package common\models\query
 * @method Order one($db = null)
 * @method Order[] all($db = null)
 */
class OrderQuery extends ActiveQuery
{

}
