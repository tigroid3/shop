<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Category;
use common\models\Item;

/**
 * Class ItemQuery
 * @package common\models\query
 * @method Item one($db = null)
 * @method Item[] all($db = null)
 */
class ItemQuery extends ActiveQuery
{
    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere([Item::tableName() . '.id' => $id]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function byCategory($id)
    {
        return $this->andWhere([Item::tableName() . '.category_id' => $id]);
    }

    /**
     * @return $this
     */
    public function notRoot()
    {
        return $this->andWhere(['!=', Item::tableName() . '.id' => Category::ROOT_CATEGORY]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function byBrand($id)
    {
        return $this->andWhere([Item::tableName() . '.brand_id' => $id]);
    }
}
