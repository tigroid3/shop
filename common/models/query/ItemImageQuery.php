<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\ItemImage;

/**
 * Class ItemImageQuery
 * @package common\models\query
 * @method ItemImage one($db = null)
 * @method ItemImage[] all($db = null)
 */
class ItemImageQuery extends ActiveQuery
{

}
