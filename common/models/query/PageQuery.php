<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Page;

/**
 * Class PageQuery
 * @package common\models\query
 * @method Page one($db = null)
 * @method Page[] all($db = null)
 */
class PageQuery extends ActiveQuery
{
    /**
     * @param $alias
     * @return $this
     */
    public function byAlias($alias)
    {
        return $this->andWhere([Page::tableName() . '.alias' => $alias]);
    }
}
