<?php
namespace common\models\query;

use common\components\db\ActiveQuery;
use common\models\Article;

/**
 * Class ArticleQuery
 * @package common\models\query
 * @method Article one($db = null)
 * @method Article[] all($db = null)
 */
class ArticleQuery extends ActiveQuery
{

}
