<?php
namespace common\helpers;

use Imagine\Image\Box;
use Yii;
use yii\helpers\Url;
use yii\imagine\Image as ImagineImage;
use yii\web\ForbiddenHttpException;

/**
 * Class Image
 * @package common\helpers
 */
class Image
{
    const SUB_DIR_ITEMS = 'items';
    const SUB_DIR_ITEMS_ORIGINAL = 'items-original';
    const SUB_DIR_CATEGORIES = 'categories';
    const SUB_DIR_BRANDS = 'brands';
    const SUB_DIR_NEWS = 'news';

    const PFX_SMALL = '_sm';
    const PFX_MEDIUM = '_md';
    const PFX_BIG = '_bg';

    const NO_PHOTO = 'no-photo.png';
    const PHOTO_WATERMARK = 'watermark.png';

    /**
     * @return string
     */
    public static function getUniqName()
    {
        return md5(uniqid() . microtime());
    }

    /**
     * @param $fileName
     * @param $prefix
     * @return string
     */
    public static function getNameWithPrefix($fileName, $prefix)
    {
        $explode = explode('.', $fileName);
        $ext = array_pop($explode);

        return $explode[0] . $prefix . '.' . $ext;
    }

    /**
     * @param $fileName
     * @return mixed
     */
    public static function getExtFile($fileName)
    {
        $explode = explode('.', $fileName);
        $ext = array_pop($explode);

        return $ext;
    }

    /**
     * @param $fileName
     * @param bool $prepareDir
     * @param null $rootSubDir
     * @param bool $withFilename
     * @return string
     */
    public static function preparePathUpload($fileName, $prepareDir = false, $rootSubDir = null, $withFilename = false)
    {
        $subDir = substr($fileName, 0, 2);

        $rootSubDirName = "";

        if ($rootSubDir) {
            $rootSubDirName = $rootSubDir . DIRECTORY_SEPARATOR;
        }

        $dir = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . $rootSubDirName . $subDir . DIRECTORY_SEPARATOR;

        if ($prepareDir) {
            self::prepareDir($dir);
        }

        if ($withFilename) {
            $dir .= $fileName;
        }

        return $dir;
    }

    /**
     * @param string $dir
     * @throws ForbiddenHttpException
     */
    public static function prepareDir($dir)
    {
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true)) {
                throw new ForbiddenHttpException(Yii::t('common', 'Не удалось создать папку "{dir}". Проверьте права доступа.', [
                    'dir' => $dir,
                ]));
            } else {
                chmod($dir, 0777);
            }
        }
    }

    /**
     * @param null $fileName
     * @param null $rootSubDir
     * @param bool $checkExist
     * @return string
     */
    public static function getUrlImage($fileName = null, $rootSubDir = null, $checkExist = true)
    {
        $subDir = substr($fileName, 0, 2);

        $rootSubDirName = "";

        if ($rootSubDir) {
            $rootSubDirName = $rootSubDir . DIRECTORY_SEPARATOR;
        }

        $prepareFileName = Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . $rootSubDirName . $subDir . DIRECTORY_SEPARATOR . $fileName;

        if (($fileName && file_exists($prepareFileName)) || !$checkExist) {
            $imageName = Url::to('/uploads/' . $rootSubDirName . $subDir . '/' . $fileName);
        } else {
            $imageName = Url::to('/uploads/' . self::NO_PHOTO);
        }

        return $imageName;
    }

    /**
     * @param $pathFile
     * @param $pathSave string
     * @param $width integer
     * @param $height integer
     * @param $proportional bool
     * @param $watermark bool
     * @param $waterX integer|bool
     * @param $waterY integer|bool
     * @param $resize float
     *
     * @return bool|string
     */
    public static function imageResize($pathFile, $pathSave, $width, $height, $proportional = true, $watermark = false, $waterX = false, $waterY = false, $resize = 0.7)
    {
        if (empty($pathFile)) {
            return true;
        }

        if (!file_exists($pathFile)) {
            return false;
        }

        $img = ImagineImage::getImagine()->open($pathFile);
        $originalHeight = $img->getSize()->getHeight();
        $originalWidth = $img->getSize()->getWidth();

        if ($proportional) {
            //проверяем вертикальная или горизонтальная
            if ($originalWidth > $originalHeight) {
                $ratio = $originalHeight / $originalWidth;

                $height = round($width * $ratio);
            } else {
                $ratio = $originalWidth / $originalHeight;

                $width = round($height * $ratio);
            }
        }

        $imageBoxSize = new Box($width, $height);
        $img->resize($imageBoxSize);

        if ($watermark) {
            $watermarkResource = ImagineImage::getImagine()->open(Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR . self::PHOTO_WATERMARK);
            $watermarkWidth = $watermarkResource->getSize()->getWidth();
            $watermarkHeight = $watermarkResource->getSize()->getHeight();
            $ratioWatermark = $watermarkHeight / $watermarkWidth;

            $diffX = $waterX > 0 ? $waterX : $width - $watermarkWidth + $waterX;
            $diffY = $waterY > 0 ? $waterY : $height - $watermarkHeight + $waterY;

            if ($width - $watermarkWidth - abs($waterX) < 0) {
                $watermarkWidth = round($width * $resize);
                $watermarkHeight = round($ratioWatermark * $watermarkWidth);

                $diffX = $waterX > 0 ? $waterX : $width - $watermarkWidth + $waterX;
                $diffY = $waterY > 0 ? $waterY : $height - $watermarkHeight + $waterY;

                $watermarkResource->resize(new Box($watermarkWidth, $watermarkHeight));
            }

            if ($diffY > 0) {
                ImagineImage::watermark($img, $watermarkResource, [$diffX, $diffY]);
            }
        }

        $img->save($pathSave);

        return true;
    }

    /**
     * @param $pathFile
     * @param $pathSave
     * @param $width
     * @param $height
     * @return bool
     */
    public static function imageThumb($pathFile, $pathSave, $width, $height)
    {
        if (empty($pathFile)) {
            return true;
        }

        if (!file_exists($pathFile)) {
            return false;
        }

        ImagineImage::thumbnail(
            $pathFile,
            $width,
            $height
        )->save($pathSave);

        return true;
    }
}
