<?php
namespace common\helpers;

use common\models\Category;
use Yii;
use yii\db\Connection;

/**
 * Class MyHelper
 * @package common\helpers
 */
class MyHelper
{
    /**
     * Перевод в транслит
     * @param $string
     * @return mixed|string
     */
    public static function translate($string)
    {
        $string = (string)$string;
        $string = strip_tags($string);
        $string = str_replace(["\n", "\r"], " ", $string);
        $string = preg_replace("/\s+/", ' ', $string);
        $string = trim($string);
        $string = function_exists('mb_strtolower') ? mb_strtolower($string) : strtolower($string);
        $string = strtr($string, ['а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'j', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ы' => 'y', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', 'ъ' => '', 'ь' => '']);
        $string = preg_replace("/[^0-9a-z-_ ]/i", "", $string);
        $string = str_replace(" ", "-", $string);

        return $string;
    }

    /**
     * Построить дерево категорий
     * @param $array
     * @param int $parent
     * @return array
     */
    public static function buildCategoryTree($array, $parent = 1)
    {
        $content = [];

        foreach ($array as $category) {
            if ($category['parent_id'] == $parent) {
                $content += [$category['id'] => $category];

                foreach ($array as $subCategory) {
                    if ($subCategory['parent_id'] == $category['id']) {
                        $content[$category['id']] += array_merge($subCategory, ['childs' => self::buildCategoryTree($array, $category['id'])]);
                        break;
                    }
                }
            }
        }

        return $content;
    }
}
