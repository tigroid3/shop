<?php
namespace console\components\db;

use console\components\db\mysql\ColumnSchemaBuilder;
use console\components\db\mysql\Schema;
use Yii;
use yii\db\Migration as YiiMigration;

/**
 * Class Migration
 * @package console\components\db
 */
class Migration extends YiiMigration
{
    const CASCADE = 'CASCADE';
    const RESTRICT = 'RESTRICT';
    const SET_NULL = 'SET NULL';
    const NO_ACTION = 'NO ACTION';

    /**
     * @var null|string
     */
    protected $tableOptions = null;

    /**
     * @var \yii\rbac\DbManager
     */
    protected $authManager = null;

    /**
     * @var string
     */
    protected $pkPrefix = 'PK';

    /**
     * @var string
     */
    protected $idxPrefix = 'IDX';

    /**
     * @var string
     */
    protected $uniPrefix = 'UNI';

    /**
     * @var string
     */
    protected $fkPrefix = 'FK';

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        $this->db->enableSchemaCache = false;

        $this->authManager = Yii::$app->getAuthManager();
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param string $columns
     */
    public function addPrimaryKey($name, $table, $columns)
    {
        if (is_null($name)) {
            $name = $this->getPkName($table, $columns);
        }

        parent::addPrimaryKey($name, $table, $columns);
    }

    /**
     * @param $table
     * @param $columns
     * @return string
     */
    public function getPkName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf(
            '%s_%s',
            $this->pkPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param $columns
     * @return string
     */
    public function includePrimaryKey($columns)
    {
        $columns = is_array($columns) ? implode(',', $columns) : $columns;

        return ('PRIMARY KEY (' . $columns . ')');
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param array|string $columns
     * @param boolean|false $unique
     */
    public function createIndex($name, $table, $columns, $unique = false)
    {
        if (is_null($name)) {
            if ($unique) {
                $name = $this->getUniName($table, $columns);
            } else {
                $name = $this->getIdxName($table, $columns);
            }
        }

        parent::createIndex($name, $table, $columns, $unique);
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getUniName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf(
            '%s_%s',
            $this->uniPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getIdxName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(sprintf(
            '%s_%s',
            $this->idxPrefix,
            $table . '_' . implode('', (array)$columns)
        ));
    }

    /**
     * @param string|null $name
     * @param string $table
     * @param string $columns
     * @param array|string $refTable
     * @param string $refColumns
     * @param null $delete
     * @param null $update
     */
    public function addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        if (is_null($name)) {
            $name = $this->getFkName($table, $columns);
        }

        parent::addForeignKey($name, $table, $columns, $refTable, $refColumns, $delete, $update);
    }

    /**
     * @param string $table
     * @param string|array $columns
     * @return string
     */
    public function getFkName($table, $columns)
    {
        $table = $this->db->schema->getRawTableName($table);

        return strtolower(substr(sprintf(
            '%s_%s_%s',
            $this->fkPrefix,
            $table,
            $columns
        ), 0, 64));
    }
}
